<?php $this->beginContent('@app/views/layouts/main.php'); ?>

	<div class="content container-fluid">
	    <?= $content ?>
	</div>

<?php $this->endContent(); ?>

