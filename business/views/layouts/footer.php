<?php
use yii\helpers\Html;
use yii\helpers\Url;

$footer[] = ['label' => Yii::t('commonTheme', 'Privacy'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/privacy')) :  Yii::$app->homeUrl];
$footer[] = ['label' => Yii::t('commonTheme', 'Terms'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/terms')) :  Yii::$app->homeUrl];
$footer[] = ['label' => Yii::t('commonTheme', 'Contact'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/contact')) :  Yii::$app->homeUrl ];
?>

<div class="footer-wrapper container-fluid center-block no-print">

    <div class="footer row">

		<div class="col-sm-4">
			&copy; <?= date('Y') ?> SwapWink. <?= Yii::t('commonTheme', 'All rights reserved') ?>.
		</div>
                
		<div class="col-sm-8">

			<ul class="list-inline">
				<?php foreach ($footer as $counter => $item): ?>
					   
					<?= Html::tag('li', Html::a($item['label'], $item['url']), []); ?>
					
					<?php if($counter < (sizeof($footer)-1)): ?>

						<?= Html::tag('li', '|', []); ?>

					<?php endif; ?>

				<?php endforeach; ?>
			</ul>

		</div>
    </div>

</div> <!-- ENDS FOOTER -->
