<?php
use common\models\Affiliate;
use common\models\User;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

if (!Yii::$app->user->isGuest):
    
    $optionsConfig = [
        'label' => Yii::t('commonTheme', 'Configuration'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('optionsConfig'),
        'icon' => 'fa-cog',
    ];

    $users = [
        'label' => Yii::t('commonTheme', 'Users'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
        'icon' => 'fa-user',
    ];

    $affiliates = [
        'label' => Yii::t('commonTheme', 'Affiliates'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
        'icon' => 'fa-building',
    ];

    $notifications = [
        'label' => Yii::t('commonTheme', 'Notifications'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('type-notification'),
        'icon' => 'fa-bell',
    ];
    
    $subsidiary = [
        'label' => Yii::t('commonTheme', 'Locations'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('subsidiary'),
        'icon' => 'fa-simplybuilt',
    ];

    $interviewers = [
        'label' => Yii::t('commonTheme', 'Pollsters'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('interviewer'),
        'icon' => 'fa-users',
    ];

    $surveys = [
        'label' => Yii::t('commonTheme', 'Surveys'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('survey'),
        'icon' => 'fa-check',
    ];

    $products = [
        'label' => Yii::t('commonTheme', 'Products'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('product'),
        'icon' => 'fa-database',
    ];
    
    $codes = array(
        'label' => Yii::t('commonTheme', 'Validate code'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon'),
        'icon' => 'fa-barcode',
    );
    
    $promoReport = [
        'label' => Yii::t('commonTheme', 'Promotions report'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('/survey-promotion/report'),
        'icon' => 'fa-tag',
    ];
    
    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Profile'),
        'url' => Yii::$app->user->identity->user_type == User::AFFILIATE ? Yii::$app->urlManager->createUrl(['affiliate/view', 'id' => Yii::$app->user->identity->affiliate_id]) : Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
        'icon' => 'fa-user'
    ];
    
    switch (Yii::$app->user->identity->user_type):
        case User::ADMIN:
            $controlPanelMenu[] = $users;
            $controlPanelMenu[] = $affiliates;
            $controlPanelMenu[] = $products;
            $controlPanelMenu[] = $notifications;
            $controlPanelMenu[] = $optionsConfig;
          
            break;
        case User::AFFILIATE:
            $affiliate = Affiliate::findOne(\Yii::$app->user->identity->affiliate_id);
            
            $controlPanelMenu[] = $interviewers;
            $controlPanelMenu[] = $surveys;
            $controlPanelMenu[] = $codes;
            $controlPanelMenu[] = $promoReport;
            $controlPanelMenu[] = $subsidiary;
            $controlPanelMenu[] = [
                'label' => Yii::t('commonTheme', 'Credits'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('credit'),
                'icon' => 'fa-credit-card'
            ];
        
            break;
        case User::INTERVIEWER:
            
            $controlPanelMenu[] = $surveys;
            $controlPanelMenu[] = $codes;
            
            break;
    endswitch;
    
    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'fa-sign-out',
    ];
    
endif;
?>

<div class="navbar-wrapper navbar no-print">

    <div class="navbar-head">

        <a id="home_url" href="<?= Yii::$app->homeUrl ?>">
            <?= Html::img(Yii::$app->params['cdnPath'] . "/img/logo-header.png", ['class' => 'navbar-brand', 'alt' => Yii::$app->name]); ?>
        </a>
        
        <?php if (!Yii::$app->user->isGuest): ?>    
        
            <div class="control-panel pull-right">

                <div class="navbar-right user-info button_switch">
                    <p class="user-name pull-left"><i class="fa fa-caret-down"></i></p>

                    <div class="user-avatar pull-left">
                        <?php
                        if (\Yii::$app->user->identity->user_type == User::AFFILIATE):
                            echo Html::img($affiliate->urlLogo, ['id' => 'avatar', 'class' => 'user-avatar']);
                        else:
                            echo Html::img(Yii::$app->user->identity->urlAvatar, ['id' => 'avatar_profile', 'class' => 'user-avatar']);
                        endif;
                        ?>
                    </div>
                </div>

                <div class="drop-list">
                    <ul>
                        <?php
                        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
                        if (count($modelUser->affiliates) != 0 || count($modelUser->subsidiaries) != 0):
                            ?>

                            <li><h2><?php echo Yii::t("commonTheme", "Use SwapWink as:"); ?></h2></li>

                            <?php if (count($modelUser->affiliates) != 0): ?>
                                <?php
                                foreach ($modelUser->affiliates as $affiliate):
                                    if (Yii::$app->user->identity->affiliate_id != $affiliate->id):
                                        if ((isset(Yii::$app->params['show-menu-core']) && $affiliate->status == Affiliate::ACTIVE) || (isset($affiliate->status_module) && $affiliate->status_module == Affiliate::STATUS_ACTIVE)):
                                            ?>
                                            <li>
                                                <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['affiliate/set', 'id' => $affiliate->id]) ?>" ><?php echo $affiliate->affiliate_name; ?></a>
                                            </li>
                                            <?php
                                        endif;
                                    endif;
                                endforeach;
                                ?>

                                <?php endif;
                            ?>

                            <?php
                            if (count($modelUser->subsidiaries) != 0):
                                foreach ($modelUser->subsidiaries as $subsidiary):
                                    if (Yii::$app->user->identity->subsidiary_id != $subsidiary->id):
                                        ?>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['subsidiary/set', 'id' => $subsidiary->id]) ?>" >Admin. <?php echo $subsidiary->subsidiary_name; ?></a>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                            endif;
                            ?>

                                <?php if (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->subsidiary_id != null): ?>
                                <li>
                                    <a href="<?php echo (Yii::$app->user->identity->subsidiary_id == null) ? Yii::$app->urlManager->createAbsoluteUrl('affiliate/unset') : Yii::$app->urlManager->createAbsoluteUrl('subsidiary/unset'); ?>"><?php echo Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name; ?></a>
                                </li>
                                <?php endif;
                            ?>
                            <?php endif;
                        ?>

                        <?php echo Html::tag('li', '', ['class' => 'separator']); ?>

                        <?php
                        foreach ($controlPanelMenu as $item):
                            if (!isset($item['separator'])):
                                if (!empty($item['url'])):
                                    $linkLabel = '<span class="fa ' . $item['icon'] . '"></span>' . $item['label'];
                                    echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                                else:
                                    echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                                endif;
                            else:
                                echo Html::tag('li', '', ['class' => 'separator']);
                            endif;
                        endforeach;
                        ?>
                    </ul>
                    <span class="arrow"></span>
                </div><!--.drop-list-->
            </div>
        
        <?php endif; ?>

    </div>

</div> <!-- End nav-bar -->


<div class="container">
    <div id="breadcrumbs" class="col-md-12">
        <?= Breadcrumbs::widget([
            'homeLink' => [ 
                      'label' => Yii::t('commonTheme', 'Home'),
                      'url' => Yii::$app->homeUrl,
                    ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>
