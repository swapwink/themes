<?php

use common\widgets\Alert;
use swapwink\themes\AppAssetBusiness;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAssetBusiness::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage ?>" lang="<?= Yii::$app->language ?>">
<?= $this->render('//layouts/header') ?>
<?php if (in_array($this->context->action->id, array("login"))):?>
<body class="body-login">
    <?php $this->beginBody() ?>
        <?php echo $content; ?>
            <div class="footer-login">
                
                <div class="footer row">
                
                    <div class="col-sm-4">
                        &copy; <?= date('Y') ?> SwapWink&reg;. <?= Yii::t('commonTheme', 'All rights reserved') ?>.
                    </div>

                    <?php 
                        $footer[] = ['label' => Yii::t('commonTheme', 'Privacy'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/privacy')) :  Yii::$app->homeUrl];
                        $footer[] = ['label' => Yii::t('commonTheme', 'Terms'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/terms')) :  Yii::$app->homeUrl];
                        $footer[] = ['label' => Yii::t('commonTheme', 'Contact'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/contact')) :  Yii::$app->homeUrl ];
                    ?>
                    <div class="col-sm-8">
                        <ul class="list-inline">
                            <?php foreach ($footer as $counter => $item): ?>

                                <?= Html::tag('li', Html::a($item['label'], $item['url']), []); ?>

                                <?php if($counter < (sizeof($footer)-1)): ?>

                                    <?= Html::tag('li', '|', []); ?>

                                <?php endif; ?>

                            <?php endforeach; ?>
                        </ul>
                    </div>
            
                </div>
            
            </div>
    <?php $this->endBody() ?>
</body>
<?php else:?>
<body>
    <?php $this->beginBody() ?>
        <?= $this->render('//layouts/navhead') ?>
        <?= Alert::widget() ?>
    <div class="content container">
        <?= $content ?>
    </div>
	<?= $this->render('//layouts/footer') ?>
    <?php $this->endBody() ?>
</body>
<?php endif;?>
</html>
<?php $this->endPage() ?>
