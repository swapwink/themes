/**
 * Se utiliza en el listado de encuestas para cambiar de estatus
 * las encuestas.
 */
$(document).on('click', '.change_status', function(e) {
    
    e.preventDefault();
    
    $('.errorMessage').html('');
    $('.errorMessage').css('display','none');
    
    var newStatus = $(this).attr('data-status');    
    var surveyId = $(this).parent().parent().attr('data-id');
    var ajaxLink = $(this).parent().parent().attr('data-link');
    
                $.ajax({
			type: 'POST',
			url: ajaxLink,
			dataType: 'json',
                        data:{ status:newStatus, id:surveyId },
			success: function(data){
                            
                            if(data.success == 'false'){
                                
                                $('.errorMessage').html('');
                                $('.errorMessage').css('display','none');
                                $('#survey_' + surveyId + ' .errorMessage').css('display','block');
                                
                                $.each(data.errors, function(index, value) {
                                   
                                    $('#survey_' + surveyId + ' .errorMessage').append(value + "<br>").hide().fadeIn();

                                });
                                
                            }
                            else{
                                
                                $('#survey_' + surveyId + ' .buttons').html("").append(data.buttons).hide().fadeIn();
                                $('#survey_' + surveyId + ' .status').html("").append(data.newStatus).hide().fadeIn();

                                
                            }
                            
			}
                        
		});
});