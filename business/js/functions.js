$.fn.createSelect2 = function() {

		function format(item, container) 
		{
		    if(item.id == 'create')
		    {
			    return '<a href="#" class="choosen_create panel-trigger" data-subpanel="true">'+item.text+'</a>';
			} else {
				return '<p class="choosen_item">'+item.text+'</p>';
			}
		}

		var selects = this;

		selects.each(function(index, el)
		{
			var this_element    = $(el);
			var placeholder 	= this_element.attr('placeholder');

			var thisChoosenConfig = {
				allowClear         : true,
				formatResult       : format,
				formatSelection    : format,
				formatInputTooShort: 'Ingrese palabras clave...',
				formatLoadMore     : 'Cargando resultados...',
				closeOnSelect: false,
			};


			if(placeholder !== undefined && thisChoosenConfig.allowClear)
			{
				this_element.prepend('<option></option>');

				if(!this_element.find('option[selected]').length)
				{
					this_element.prop("selectedIndex", 0);
				}	
			}
			
			if (typeof this_element.select2 == 'function'){
				this_element.select2(thisChoosenConfig);
			}
		});
};

$(document).ready(function(){

	var body	= $('body'),
	content		= $('.content');


    /* *****					  		*****************************************************				*/
    /* #####	DROPDOWN LIST 	 		#####################################################				*/
    /* *****	 						*****************************************************				*/

	body.on('click', '.button_switch', function(e)
	{
		e.stopPropagation();
		e.preventDefault();

		var this_element	= $(this);
		var drop			= this_element.parent().find('.drop-list');

		if(this_element.hasClass('active'))
		{
			this_element.removeClass('active');
		} else{
			var anotherActive     = $('.button_switch.active');
			var dropAnotherActive = anotherActive.parent().find('.drop-list');

			if(anotherActive.length)
			{
				anotherActive.removeClass('active');
				dropAnotherActive.fadeToggle('fast');
			}

			this_element.addClass('active');
		}

		drop.fadeToggle('fast');
	});

	/* Notificaciones */

	body.on('click', '#notifications-icon', function()
	{
		var globito = $(this).find('span');

		if(globito.length)
		{
			globito.remove();
			// Update de notificaciones como leídas
	    }
    });


	//Cerrar el cuadro de búsqueda cuando se haga click fuera

	body.click(function() 
	{
		var button_switch	= $(".button_switch.active"),
		drop				= button_switch.parent().find('.drop-list');

		button_switch.removeClass("toggled");
		drop.fadeToggle('fast');
		button_switch.removeClass('active');
	});


    /* *****	FUNCIÓN GLOBAL	  		*****************************************************				*/
    /* #####	PARA CAMBIAR CLASE 		#####################################################				*/
    /* *****	ACTIVE EN CLICK	 		*****************************************************				*/


    body.on('click', '.switch_option', function(event)
    {
		var this_element	= $(this);
		var brother_active	= this_element.parent().find('.active');

		brother_active.removeClass('active');
		this_element.addClass('active');

    });

    /* *****					  		*****************************************************				*/
    /* #####	PANEL INTERACTIONS 	 	#####################################################				*/
    /* *****	 						*****************************************************				*/

	$(window).bind('resize', function(event) 
	{
		updatePanelHeight();

		var popupDialog = $( ".popup" );

		if(popupDialog.length)
		{
			popupDialog.dialog( "option", "position", { my: "center", at: "center", of: window } );
		}

	});


	function updatePanelPosition(callback)
	{
		var panels = $('.proces-panel');

		if(panels.length) 
		{
			panels.each(function()
			{	
				var this_element = $(this);
				if($('.navbar-fixed-top').length)
				{
					this_element.css('position','fixed')
					.stop(false, true).animate({ top: '67px', height: '86%' },0);

				} else {
					this_element.stop(false, true).animate({top: '125px'}, 0, function(){
						this_element.css('position','');

						if(callback !== undefined)
						{
							callback();
						}
					});		
				}
			});
		}	

	}

	function updatePanelHeight(callback)
	{
		var panels = $('.proces-panel');

		if( panels.length)
		{
			panels.each(function()
			{
				var this_element = $(this);
				var topPosition = this_element.css('top');
				topPosition = topPosition.replace('px','');
				topPosition = topPosition.replace('-','');

				this_element.stop(false, true).animate({
					height: (($(window).height()-topPosition)-5)+'px'},
					400,
					function() { 

						if(callback !== undefined)
						{
							callback();
						}
			   	});
			});
		}
	}

	/* *****	TOOLTIPS EN DIFERENTES  	*****************************************************				*/
	/* #####	DIRECCIONES VISTAS			#####################################################				*/
	/* *****	SELCTOR DE CLASES 			*****************************************************				*/


	var someTooltip = $('[class*="mws-tooltip"]');

	if(someTooltip.length)
	{
		var gravity = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];

		for(var i in gravity) 
		{
			$(".mws-tooltip-"+gravity[i]).tipsy({
				gravity: gravity[i], 
				live: true, 
				delayIn: 250, 
				offset: 7,
				fade: true,
				opacity: .8
			});
		}

		body.on('click', '.tipsy', function(event)
		{
			$(this).remove();
		});
	}


	/* *****	  							*****************************************************				*/
	/* #####	INPUTS TIPO DATE			#####################################################				*/
	/* *****								*****************************************************				*/

	function createInputDate()
	{
		var inputsDate = $(".datepicker");

		if(inputsDate.length)
		{
			inputsDate.each(function()
			{
				var this_element = $(this);

	   			this_element.datepicker({
			        currentText: 'Hoy',
			        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
			        dateFormat: 'yy-mm-dd',
			        showOtherMonths:true,
			        beforeShow: function(input, obj)
			        {
						this_element.addClass('data_picker_active');
			        },
			        onClose: function(input, obj)
			        {
						this_element.removeClass('data_picker_active');
			        }
				});

				var datePickerId		= this_element.attr('id');
				var datePickerTrigger	= this_element.siblings('.datepicker_trigger');

	   			datePickerTrigger.attr('datepicker-trigger', datePickerId);

				body.on('click', '[datepicker-trigger='+datePickerId+']', function(e) 
				{

				    if (this_element.datepicker('widget').is(':hidden')) 
				    {
				        this_element.datepicker("show");
				    } else {
				        this_element.mouseout();
				    }
				});

			});
		}
	}

    /* *****							*****************************************************				*/
    /* #####	CHOOSEN WITH AJAX		#####################################################				*/
    /* *****				 			*****************************************************				*/

	function createChoosens()
	{		
		$('select').createSelect2();
	}

    /* *****							*****************************************************				*/
    /* #####	NUMERIC SPINNERS		#####################################################				*/
    /* *****				 			*****************************************************				*/


    function createNumericSpinners() 
    {
    	var numericSpinners = $('.numeric_spinner');

    	if(numericSpinners.length)
    	{
    		numericSpinners.each(function(index, el) 
    		{
    			var this_element = $(this);

    			if(!this_element.hasClass('ui-spinner-input'))
    			{
    				

					var maxValue	= this_element.attr('data-max');
					var minValue	= this_element.attr('data-min');
					var step		= this_element.attr('data-step');
					var format		= this_element.attr('data-format');
					var disabled	= this_element.attr('data-disabled');

					var numericSpinnerConfig	= {
						culture: "en-US"
					};

    				if(maxValue !== undefined)
    				{
    					numericSpinnerConfig.max = maxValue;
    				}

					if(minValue	!== undefined)
    				{
    					numericSpinnerConfig.min = minValue;
					}

 					if(step !== undefined)
    				{
    					numericSpinnerConfig.step = step;

    					numericSpinnerConfig.change = function( event, ui ) 
    					{
							var thisSpinner		= $(this);
							var spinnerValue	= this_element.spinner('value');

    						if($.isNumeric(spinnerValue))
    						{
    							if((spinnerValue % step) !== 0)
    							{
    								// this_element.spinner( "stepUp", 1 );
    							}
    						} else {
    							this_element.val(1);
    							this_element.spinner( "value", 1);
    						}
    					}

    				}

 					if(format !== undefined)
    				{
    					numericSpinnerConfig.numberFormat = format;
    				}

    				this_element.spinner(numericSpinnerConfig);

					if(disabled	== 'true')
    				{
    					this_element.siblings('.ui-spinner-button').css('display', 'none');

						this_element.on('spin', function(e)
						{
							e.preventDefault();
						});
					}

					this_element.on('change', function()
					{

						var spinnerValue	= this_element.val();

						spinnerValue = spinnerValue.replace('$', '');
						spinnerValue = spinnerValue.replace(',', '');

						if($.isNumeric(spinnerValue))
						{
							if(step !== undefined)
							{
								if((spinnerValue % step) !== 0)
								{
									this_element.spinner( "stepUp", 0 );
								} else {
									this_element.spinner('value', parseInt(this_element.val()) );
								}
							}

						} else {
							this_element.val(1);
							this_element.spinner( "value",1);
						}
					});

					this_element.on('focus', function()
					{
						var thisSpinner		= $(this);
						var spinnerValue	= this_element.spinner('value');

						this_element.val(spinnerValue);
					});

					this_element.on('blur', function() 
					{
						var thisSpinner	= $(this);
						var inputValue	= this_element.val();

						inputValue = inputValue.replace('$', '');
						inputValue = inputValue.replace(',', '');

						if($.isNumeric(inputValue))
						{
							this_element.spinner( "value", inputValue);
						} else {
							this_element.val(1);
							this_element.spinner( "value", 1);
						}
					});

    			}
    		});
    	}
    }


	body.on('click', '.confirm-action', function(e){

		if(!confirm('¿Está seguro de realizar esta acción?')) {
			e.preventDefault();
		}

	});


	function initSwapComponents()
	{
		createInputDate();
		createNumericSpinners();
		createChoosens();
	}

	initSwapComponents();

	body.on('click', '.ajax-delete', function(event)
	{ 
		event.preventDefault();

		var thisLink    = $(this);
		var thisLinkUrl = thisLink.attr('href');

		//Agregar confirm

		$.ajax({
			url    : thisLinkUrl,
			data   : {},
			type   : 'POST',
			success: function(data)
			{
				closePanel();

				var gridView = $('.grid-view');

				if(gridView.length)
				{
					gridView.yiiGridView('update');
				}
			},
			error: function(xhr, ajaxOptions, thrownError)
			{
				alert(xhr.responseText);
			}
		});
	});
    
	$(".btn-print").on("click", function(){
		window.print();
		return false;
	})
	
	
	$("#all_users_survey").on("change", function(){
		if($(this).is(':checked')){
			$("#SurveyModel_interviewersAllowed").val();
			$("#SurveyModel_interviewersAllowed").select2('data', null)
		}
	});
	
	$("#SurveyModel_interviewersAllowed").on("change", function(){
		if(!$("#SurveyModel_interviewersAllowed").select2('data').length){
			$("#all_users_survey").prop( "checked", true );
		}else{
			$("#all_users_survey").prop( "checked", false );
		}
	});
	
	
	
	var $colorBox = $('.color-box');
	
	
	if($colorBox.length){
		
		$(".color-box").spectrum({
			allowEmpty: true,
			clickoutFiresChange: false,
			change: function(color) {
				if(color !== null){
					hex = color.toHex()
				}else{
					hex = '';
				}

				$.ajax({
					url    :  yii.urls.saveBGColor,
					data   : { color : hex},
					type   : 'POST',
					success: function(data){
					},
					error: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					}
				});
			},
			move: function(color) {
				dataControl = $(this).data("control");
				$elementToSync = $(dataControl);
				
				if(color !== null){
					$elementToSync.css('background-color', color.toHexString());
				}else{
					$elementToSync.css('background-color', 'transparent');
				}
			},
			hide: function(color) {
				dataControl = $(this).data("control");
				$elementToSync = $(dataControl);
				
				if(color !== null){
					$elementToSync.css('background-color', color.toHexString());
				}else{
					$elementToSync.css('background-color', 'transparent');
				}
			}
		});
	}
});

 /*
  * Evita doble click en todos formularios menos en los formularios
  * donde se realiza una llamada ajax.
  */              
 $(document).on('submit',"form:not(.submitAjax)",function() {
  
  var form = $(this);
  form.find("input[type=submit]").attr("disabled", "disabled");
  form.find("input").attr("readonly", true);
  document.body.style.cursor = "wait";
  
 });
 
/*
 * Funcion que bloquea el submit del formulario, cuando existen validaciones por 
 * ajax.
 */ 
function blockSubmit(form){
    
  form.find("input[type=submit]").attr("disabled", "disabled");
  form.find("input").attr("readonly", true);

}


/**
 *  Funcion que desbloquea el submit del formulario, cuando existen validaciones
 *   por ajax.
 */
function submitDefault(form){
  
  form.find("input[type=submit]").attr("disabled", false);
  form.find("input").attr("readonly", false);
    
}