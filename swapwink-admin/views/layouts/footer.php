<?php
use yii\helpers\Html;
use yii\helpers\Url;


$footer[] = ['label' => Yii::t('commonTheme', 'Privacy'), 'url' => Yii::$app->homeUrl];
/*
TODO: Habilitar cuando el manual esté listo.
$footer[] = ['label' => Yii::t('commonTheme', 'Manual'), 'url' => Yii::$app->homeUrl];
*/
$footer[] = ['label' => Yii::t('commonTheme', 'Contact'), 'url' => !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/contact')) :  Yii::$app->homeUrl ];
?>

<footer class="main-footer">
	<div class="col-1">
		<p>
			<?php foreach ($footer as $counter => $item):?>
				<?= Html::a($item['label'], $item['url']); ?>
				<?php if($counter < (sizeof($footer)-1)): ?>
					|
				<?php endif;?>
			<?php endforeach;?></p>
	</div>
	<div class="col-2">
                &copy; <?= date('Y'); ?> SwapWink. <?= Yii::t('commonTheme', 'All rights reserved'); ?>.
	</div>
	<div class="clearfix"></div>
</footer><!--.main-footer-->
