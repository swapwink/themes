<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->title = Yii::$app->name . ' - ' .  ucfirst(Yii::$app->controller->id) . " " . ucfirst(Yii::$app->controller->action->id);

?>
<head>
	<meta charset="<?= Yii::$app->charset;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($titlePage) ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php
		$this->head();
		$this->registerLinkTag(['rel' => 'icon', 'href' => Yii::$app->params['cdnPath'] . '/img/favicon.ico']);
	?>

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
