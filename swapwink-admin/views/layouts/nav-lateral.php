<?php

use common\models\User;
use yii\helpers\Html;

$sideMenu = [];

if (!\Yii::$app->user->isGuest):

    $users = [
        'label' => Yii::t('commonTheme', 'Users'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-user.png",
    ];

    $affiliates = [
        'label' => Yii::t('commonTheme', 'Affiliates'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png",
    ];

    $affiliatesStory = [
        'label' => Yii::t('commonTheme', 'Affiliates Stories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate-story'),
        'icon' => [
            'type' => 'font',
            'class' => 'pencil-square-o'
        ]        
    ];

    $coupons = [
        'label' => Yii::t('commonTheme', 'Coupons'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/create'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
    ];

    $rewards = [
        'label' => Yii::t('commonTheme', 'Rewards'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/create'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
    ];

    $couponsApproval = [
        'label' => Yii::t('commonTheme', 'Coupons'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/approval'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
    ];

    $rewardsApproval = [
        'label' => Yii::t('commonTheme', 'Rewards'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/approval'),
        'icon' => [
            'type' => 'font',
            'class' => 'gift'
        ]
    ];

    $reports = [
        'label' => Yii::t('commonTheme', 'Reports'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('reports'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-reports.png",
    ];

    $affiliateReports = [
        'label' => Yii::t('commonTheme', 'Reports'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate/report'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-reports.png",
    ];

    $credits = [
        'label' => Yii::t('commonTheme', 'Credits'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-card.png",
    ];

    $devices = [
        'label' => Yii::t('commonTheme', 'Devices'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('device'),
        'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-device.png",
    ];

    if (Yii::$app->id == 'swapwink/social') {
        $labelExchange = Yii::t('commonTheme', 'Manually Exchange of Rewards');
        $urlExchange = Yii::$app->urlManager->createAbsoluteUrl('reward/exchange');
    } elseif (Yii::$app->id == 'swapwink/coupon') {
        $labelExchange = Yii::t('commonTheme', 'Manually Redeem Coupons');
        $urlExchange = Yii::$app->urlManager->createAbsoluteUrl('coupon/exchange');
    }

    if (Yii::$app->id == 'swapwink/social' || Yii::$app->id == 'swapwink/coupon') {
        $exchange = [
            'label' => $labelExchange,
            'url' => $urlExchange,
            'icon' => [
                'type' => 'font',
                'class' => 'barcode'
            ]
        ];
    }

    $categories = [
        'label' => Yii::t('commonTheme', 'Coupon Categories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category'),
        'icon' => [
            'type' => 'font',
            'class' => 'list'
        ]
    ];

    $subsidiaries = [
        'label' => Yii::t('commonTheme', 'Locations'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('subsidiary'),
        'icon' => [
            'type' => 'font',
            'class' => 'users'
        ]
    ];

    $ip_management = [
        'label' => Yii::t('commonTheme', 'IP Blacklist'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('ip-blacklist'),
        'icon' => [
            'type' => 'font',
            'class' => 'ban'
        ]
    ];

    $preregister = [
        'label' => Yii::t('commonTheme', 'Pre-registered'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('preregister'),
        'icon' => [
            'type' => 'font',
            'class' => 'user-plus'
        ]
    ];

    if (Yii::$app->session->get('affiliate.isDevice') !== null) {
        $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['coupon/exchange']);
    }

    if (isset(Yii::$app->params['show-menu-core']) && Yii::$app->params['cdnPathAdmin'] == true) {
        $sideMenu[] = $users;
        $sideMenu[] = $affiliates;
        $sideMenu[] = $preregister;
        $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
    } else {

        switch (Yii::$app->user->identity->user_type):
            case User::ADMIN:
                $sideMenu[] = $users;
                $sideMenu[] = $affiliates;
                $sideMenu[] = $affiliatesStory;
                $sideMenu[] = $categories;

                if (Yii::$app->id == 'swapwink/social') {
                    $sideMenu[] = $rewardsApproval;
                } elseif (Yii::$app->id == 'swapwink/coupon' || Yii::$app->id == 'swapwink/coupon-b2b') {
                    $sideMenu[] = $couponsApproval;
                }

                $sideMenu[] = $reports;
                $sideMenu[] = $ip_management;
                $sideMenu[] = $preregister;
                if (Yii::$app->getModule('moduleManager') != null && count(Yii::$app->getModule('moduleManager')->getInstalledModules()) > 0) {
                    foreach (Yii::$app->getModule('moduleManager')->getInstalledModulesByRoleForThemes('admin') as $format) {
                        $sideMenu[] = $format;
                    }
                }
                $homeRedirect = Yii::$app->urlManagerBackend->createAbsoluteUrl(['/']);
                break;
            case User::AFFILIATE:
                if (Yii::$app->id == 'swapwink/social') {
                    $sideMenu[] = $rewards;
                } elseif (Yii::$app->id == 'swapwink/coupon') {
                    $sideMenu[] = $coupons;
                }

                $sideMenu[] = $credits;
                $sideMenu[] = $devices;
                $sideMenu[] = $exchange;
                $sideMenu[] = $subsidiaries;
                $sideMenu[] = $affiliateReports;
                $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['affiliate/dashboard']);
                break;
            case User::SUBSIDIARY:
                if (Yii::$app->id == 'swapwink/social') {
                    $sideMenu[] = $rewards;
                } elseif (Yii::$app->id == 'swapwink/coupon') {
                    $sideMenu[] = $coupons;
                }
                $sideMenu[] = $credits;
                $sideMenu[] = $devices;
                $sideMenu[] = $exchange;
                $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['affiliate/dashboard']);
                break;
            case User::DEVICE:
                $sideMenu[] = $exchange;
                break;
        endswitch;
    }
endif;
?>

<div id="adminmenuback"></div>
<div class="menu-lateral">
    <ul class="carousel">
        <?php
        foreach ($sideMenu as $item):
            if (!isset($item['separator'])):
                if (!empty($item['url'])):

                    if (isset($item['icon']['type'])):

                        $icon = "<i class='fa fa-" . $item['icon']['class'] . " fa-2'></i>";

                    else:

                        $icon = Html::img($item['icon']);

                    endif;


                    echo Html::tag('li', Html::a($icon, $item['url']), ['title' => $item['label']]);

                else:
                    echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                endif;
            else:
                echo Html::tag('li', '', ['class' => 'separator']);
            endif;
        endforeach;
        ?>
    </ul>
    <div class="clearfix"></div>
</div>
