<?php

use common\models\Affiliate;
use common\models\Subsidiary;
use common\models\User;
use yii\helpers\Html;

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest):
    if(in_array(Yii::$app->user->identity->user_type, [User::AFFILIATE, User::SUBSIDIARY]) || (defined('common\\models\\User\\DEVICE') &&  \Yii::$app->user->identity->user_type == User::DEVICE)):
		
		if (Yii::$app->user->identity->affiliate_id) :
			$affiliate = Affiliate::findOne(\Yii::$app->user->identity->affiliate_id);
		elseif (Yii::$app->user->identity->subsidiary_id) :
			$subsidiaryInfo = Subsidiary::findOne(Yii::$app->user->identity->subsidiary_id);
			$affiliate = Affiliate::findOne($subsidiaryInfo->affiliate_id);
		endif;
		
	elseif(Yii::$app->session->get('affiliate.id') != null):
		$affiliate = Affiliate::findOne(\Yii::$app->session->get('affiliate.id'));
    endif;

    
    $credit = 0;
    
    if (Yii::$app->id != 'swapwink/core') {
      
	    if (Yii::$app->user->identity->user_type == User::DEVICE) {
	      	$credit = Yii::$app->coreusers->getCredit();
	    }
      
      
    } else {
      
    	$classCredit = 'common\models\Credit';
      
      	if(class_exists($classCredit)){
        
	        $creditModel = Yii::createObject($classCredit);

	        if (Yii::$app->user->identity->user_type == User::AFFILIATE) {

	      		$credit = $creditModel->getCreditAvailableForAffiliate(Yii::$app->user->identity->affiliate_id);

	      	} else {

	      		$credit = $creditModel->getCreditAvailableForUser(Yii::$app->user->identity->id);
	      	}
      	}
      
    }
    
    /**
     * TODO: Hacer el link dinamico, correspondiente
     *       a los instalados en el modulo moduleManager.
     */
    $optionsConfig = [
            'label' => Yii::t('commonTheme', 'Configuration'),
            'url' => '#',
            'icon' => 'fa-cog',
    ];
        
    $users = [
        'label' => Yii::t('commonTheme', 'Users'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
        'icon' => 'fa-user',
    ];
        
    $affiliates = [
        'label' => Yii::t('commonTheme', 'Affiliates'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
        'icon' => 'fa-building',
    ];
        
    $notifications = [
        'label' => Yii::t('commonTheme', 'Notifications'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('typeNotification'),
        'icon' => 'fa-bell',
    ];   
        
    $categories = [
        'label' => Yii::t('commonTheme', 'Categories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category'),
        'icon' => 'fa-list',
    ];
        
    $categoriesMarket = [
        'label' => Yii::t('commonTheme', 'Category'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('categoryMarket'),
        'icon' => 'fa-cubes',
    ];
    
    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Profile'),
        'url' => Yii::$app->user->identity->user_type == User::AFFILIATE ? Yii::$app->urlManager->createUrl(['affiliate/view','id'=>Yii::$app->user->identity->affiliate_id]) : (Yii::$app->user->identity->user_type == User::SUBSIDIARY ? Yii::$app->urlManager->createUrl(['subsidiary/view','id'=>Yii::$app->user->identity->subsidiary_id]) : Yii::$app->urlManager->createAbsoluteUrl("@".Yii::$app->user->identity->alias)),
        'icon' => 'fa-user'
    ];

	$moduleManager = [
		'label' => Yii::t('commonTheme', 'Module Manager'),
		'url' => Yii::$app->urlManager->createAbsoluteUrl('//moduleManager/default/index'),
		'icon' => 'fa-download',
	];
    
	
	if(isset(Yii::$app->params['show-menu-core']) && Yii::$app->params['cdnPathAdmin'] == true){
		$controlPanelMenu[] = $users;
		$controlPanelMenu[] = $affiliates;
		$controlPanelMenu[] = $notifications;
		$controlPanelMenu[] = $categories;
		$controlPanelMenu[] = $categoriesMarket;
		$controlPanelMenu[] = $optionsConfig;
	}else{
		switch(Yii::$app->user->identity->user_type):
			case User::ADMIN:
				$controlPanelMenu[] = $users;
				$controlPanelMenu[] = $affiliates;
				$controlPanelMenu[] = $notifications;
				$controlPanelMenu[] = $categories;
				$controlPanelMenu[] = $categoriesMarket;
				$controlPanelMenu[] = $optionsConfig;
				$controlPanelMenu[] = $moduleManager;
							
			break;
			case User::PARTICIPANT:
				//TODO:menú de participante
				$controlPanelMenu[] = [
						'label' => Yii::t('commonTheme', 'Credits'),
						'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge'),
						'icon' => 'fa-money'
				];
			break;
			case User::AFFILIATE:
				//TODO:menú de afiliado
			
			break;
			case User::DEVICE:
				$controlPanelMenu = [];
			break;
		endswitch;
	}

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'fa-sign-out'
    ];
endif;
?>
<header class="main-header">
	<a href="./" class="logo-content"><?=Html::img(Yii::$app->params['cdnPathAdmin'] . "/img/logo-swap.png");?></a>

	<div class="col-left hidden-xs hidden-sm">
		<h2><?= Yii::t('commonTheme', 'Administrative Panel'); ?></h2>
		<p class="view-name"><?= Html::encode($titlePage) ?></p>
	</div>

	<div class="col-right">
          
          <?php if(!empty(Yii::$app->session->get('affiliate.id'))): ?>
            <div class="pull-left credit-amount">
              <img src="<?= Yii::$app->params['cdn'] ?>/themes/default/img/credit.png">
              <div class="amount"> <?= $credit ?> <span class="currency">swk</span> </div>
            </div>
          <?php endif; ?>
        
		<div class="control-panel pull-right">
			<div class="navbar-right user-info">
				<?php 
					if(\Yii::$app->user->identity->user_type == User::AFFILIATE || (defined('common\\models\\User\\DEVICE') &&  \Yii::$app->user->identity->user_type == User::DEVICE)){
						$name = $affiliate->affiliate_name;
					}else{
						$name = !empty(\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name : \Yii::$app->user->identity->alias;
						$name .= !empty(\Yii::$app->user->identity->last_name) ? ' '.\Yii::$app->user->identity->last_name : '';
					}?>
                <!--<p class="pull-left user-name"><span class="name"><?= ucwords($name) ?></span> <i class="fa fa-caret-down"></i></p>-->
				
				<div class="user-avatar pull-left">
					<div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
						<span class="caret"></span>
						<?php
							if(in_array(Yii::$app->user->identity->user_type, [User::AFFILIATE, User::SUBSIDIARY]) || (defined('common\\models\\User\\DEVICE') &&  \Yii::$app->user->identity->user_type == User::DEVICE) || isset($affiliate)):
								echo Html::img($affiliate->urlLogo, ['id'=>'avatar', 'class'=>'user-avatar']);
							else:
								echo Html::img(Yii::$app->user->identity->urlAvatar, ['id'=>'avatar_profile', 'class'=>'user-avatar']);
							endif;?>
					  </button>
					  <ul class="dropdown-menu" role="menu">
							<?php
								if(Yii::$app->session->get('affiliate.isDevice') == null):
									$modelUser = User::findIdentity(Yii::$app->user->identity->id);
									if(count($modelUser->affiliates) != 0 || count($modelUser->subsidiaries) != 0 ):?>

										<li><h2><?= Yii::t("commonTheme", "Use SwapWink as:");?></h2></li>
										<?php
											if(count($modelUser->affiliates) != 0):?>
												<?php
													foreach($modelUser->affiliates as $affiliate):
														if (Yii::$app->user->identity->affiliate_id != $affiliate->id && $affiliate->status_module == Affiliate::STATUS_ACTIVE):?>
															<li>
																<a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['affiliate/set', 'id' => $affiliate->id]) ?>" ><?= $affiliate->affiliate_name ?></a>
															</li>
												<?php
														endif;
													endforeach;?>

										<?php
											endif;?>
											
										<?php
											if( count($modelUser->subsidiaries) != 0 ):
												foreach($modelUser->subsidiaries as $subsidiary):						
													if (Yii::$app->user->identity->subsidiary_id != $subsidiary->id):?>
														<li role="presentation">
															<a role="menuitem" tabindex="-1" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['subsidiary/set', 'id' => $subsidiary->id]) ?>" >Admin. <?php echo $subsidiary->subsidiary_name; ?></a>
														</li>
											<?php
													endif;
												endforeach;
											endif;?>
											
										<?php
											if(Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->subsidiary_id != null ):?>
												<li>
													<a href="<?= (Yii::$app->user->identity->subsidiary_id == null) ? Yii::$app->urlManager->createAbsoluteUrl('affiliate/unset') : Yii::$app->urlManager->createAbsoluteUrl('subsidiary/unset'); ?>"><?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name; ?></a>
												</li>
										<?php
											endif;?>
								<?php
									endif;?>
									
								<?= Html::tag('li', '', ['class' => 'separator']) ?>
							<?php
								endif;?>
					  
							<?php
							foreach ($controlPanelMenu as $item):
								if (!isset($item['separator'])):
									if (!empty($item['url'])):
										$linkLabel = '<span class="fa ' . $item['icon'] . '"></span> ' . $item['label'];
										echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
									else:
										echo Html::tag('li', $item['label'], ['class' => 'text-item']);
									endif;
								else:
									echo Html::tag('li', '', ['class' => 'separator']);
								endif;
							endforeach;
							?>
					  </ul>
					</div>
				</div>
			</div><!--.user-info-->
		</div>
	</div>
	<div class="clearfix"></div>
</header><!--.main-header-->

