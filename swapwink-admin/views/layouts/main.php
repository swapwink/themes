<?php
	use swapwink\themes\AppAssetAdmin;
	use common\widgets\Alert;
	AppAssetAdmin::register($this);
        
        $this->beginPage(); ?>


<?php $titlePage = $this->title; ?>
<?php
    if(isset($this->params['pageTitle'])){
        $this->title = $this->params['pageTitle'];
    }
?>

<!DOCTYPE html>

<?=
$this->render('//layouts/header', [
    'titlePage' => $titlePage
])
?>

<body>
	<?php $this->beginBody() ?>
		<!--[if lt IE 7]>
			<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

                <div class="outer-wrapper">
                    <?= $this->render('//layouts/nav-lateral') ?>
                    <div class="wrapper">
                        <?=
                        $this->render('//layouts/navhead', [
                            'titlePage' => $titlePage
                        ])
                        ?>

                        <div role="main" id="main-wrap">
                            <?= Alert::widget() ?>
                            <?= $content; ?>
                        </div>

                        <?= $this->render('//layouts/footer') ?>
                    </div><!--.wrapper-->
                </div><!--.outer-wrapper-->
		
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
