$(document).ready(function() {
    var $carousel;
    var isFlickity = false;

    function switchFlickity() {
        $w = $(window).width();
        if ($w < 600) {
            $carousel = $('.carousel').flickity({
                cellAlign: 'left',
                prevNextButtons: false,
                pageDots: false
            });
            isFlickity = true;
        } else if (isFlickity && $w >= 600) {
            $carousel.flickity('destroy');
            isFlickity = !isFlickity;
        }
    }

    $(window).on({
        "load": function() {
            switchFlickity();
        },
        "resize": function() {
            switchFlickity();
        }
    });

    $(".nmt").each(function() {
        var nmtTable = $(this);
        var nmtHeadRow = nmtTable.find("thead tr");
        nmtTable.find("tbody tr").each(function() {
            var curRow = $(this);
            for (var i = 0; i < curRow.find("td").length; i++) {
                var rowSelector = "td:eq(" + i + ")";
                var headSelector = "th:eq(" + i + ")";
                curRow.find(rowSelector).attr('data-title', nmtHeadRow.find(headSelector).text());
            }
        });
    });
});