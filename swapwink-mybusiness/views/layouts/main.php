<?php
use swapwink\themes\AssetMybusinessFrontend;
use common\widgets\Alert;

AssetMybusinessFrontend::register($this);

$this->beginPage();
$this->title = isset($this->title) ? $this->title : 'Swapwink Mybusiness';
?>
<!DOCTYPE html>
<?= $this->render('//layouts/header', ['titlePage' => $this->title])?>
<body class="<?= !empty(Yii::$app->view->params['bodyClass']) ? Yii::$app->view->params['bodyClass'] : '' ?>">
    <?php $this->beginBody() ?>

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="outer-wrapper">
        <?= $this->render('//layouts/nav-lateral') ?>
        <div class="wrapper">
            <?= $this->render('//layouts/navhead', ['titlePage' => $this->title]) ?>

            <div role="main" id="main-wrap">
                <?= Alert::widget() ?>
                <?= $content ?>
                <div class="clearfix"></div>
            </div>
            <?= $this->render('//layouts/footer') ?>
        </div><!--.wrapper-->
    </div><!--.outer-wrapper-->

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
    attribution=setup_tool
    page_id="1077909282366846"
    theme_color="#20dc9c"
    logged_in_greeting="<?=Yii::t('base', 'Hi!, how I can help you?')?>"
    logged_out_greeting="<?=Yii::t('base', 'Hi!, how I can help you?')?>">
    </div>

    <?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
