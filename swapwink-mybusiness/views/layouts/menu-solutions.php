<?php
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $credit */

if (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->user_type == User::SUBSIDIARY) : ?>
    <div class="dropdown hidden-md hidden-lg pull-left">
        <button id="btn-dropdown-menu-solutions" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= Yii::t('commonTheme', 'Solutions') ?>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-solutions" aria-labelledby="dLabel">
            <li>
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//coupon/sold') ?>">
                    <i class="icon-swapwink"></i> Swapwink
                </a>
            </li>
            <li>
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//reward/dashboard') ?>">
                    <i class="icon-rewards"></i> Rewards
                </a>
            </li>
            <li>
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//ask/index') ?>">
                <i class="icon-ask"></i> Ask
                </a>
            </li>
            <?php
            /* TODO: Habilitar cuando esté disponible la nueva versión de Contest
            <li>
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//contest/index') ?>">
                    <i class="icon-contest"></i> Contest
                </a>
            </li>
            */
            ?>
        </ul>
    </div><!--.dropdown-->
    <div class="pull-left solutions hidden-sm hidden-xs">
        <div class="solution-square">
            <a class="link-solution <?= (Yii::$app->controller->id=="coupon") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//coupon/sold') ?>">
                <i class="icon-swapwink"></i>
                <span class="solution-label">Swapwink</span>
            </a>
        </div>
        <div class="solution-square">
            <a class="link-solution <?= (Yii::$app->controller->id=="reward") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//reward/dashboard') ?>">
                <i class="icon-rewards"></i>
                <span class="solution-label">Rewards</span>
            </a>
        </div>
        <div class="solution-square">
            <a class="link-solution <?= (Yii::$app->controller->id=="ask") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//ask/index') ?>" >
                <i class="icon-ask"></i>
                <span class="solution-label">Ask</span>
            </a>
        </div>
        <?php 
        /* TODO: Habilitar cuando esté disponible la nueva versión de Contest
        <div class="solution-square">
            <a class="link-solution <?= (Yii::$app->controller->id=="contest") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//contest/index') ?>" >
                <i class="icon-rewards"></i>
                <span class="solution-label">Contest</span>
            </a>
        </div>
        */
        ?>     
    </div><!--.solutions-->
    <?php
    $this->registerCss('
     .unread-number-btn-chat-header {
         position: absolute;
         color: #fff;
         background-color: red;
         left: 23px;
         top: -2px;
         border-radius: 12px;
         width: 12px;
         text-align: center;
     }');

    if (!empty(Yii::$app->session->get('affiliate.id'))) : ?>
        <div class="pull-left hidden-sm hidden-xs credit-amount">
            <i class="ico-wallet"></i> $<?= $credit ?> <span class="currency">MXN</span>
        </div>
    <?php
    endif;?>

    <div class="notifications">
        <div class="dropdown dropdown-messages">
            <button class="btn btn-default dropdown-toggle" type="button" >
                <?=Html::a(Yii::t('models/Coupon', '<i class="icon-message"></i>'), Url::to(['coupon/sold']))?>
                <span class="badge"></span>
            </button>
            <span id="chat-unreadtotal-icon" class="unread-number-btn-chat-header"></span>
        </div>
        
        <div class="dropdown dropdown-notification">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                <i class="icon-bell"></i>
                <span class="badge"></span>
            </button>
        </div>
    </div>
<?php
endif; ?>