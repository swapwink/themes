<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
?>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($titlePage . ' - Swapwink Mybusiness')  ?></title>
    <?php
    $this->head();
    $this->registerLinkTag(['rel' => 'icon', 'href' => Yii::$app->params['cdnPath'] . '/img/favicon.ico']);
    ?>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
