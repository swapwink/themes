<?php
use common\models\User;
use swapwink\themes\MenuHelper;

$sideMenu = [];

if (!\Yii::$app->user->isGuest) :
    $solutions =[
        'coupon' => [
            $sold = [
                'label' => Yii::t('commonTheme', 'Sold Products'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/sold'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-sale'
                ],
                'requestedRoute' => ['coupon/sold']
            ],
            $couponReports = [
                'label' => Yii::t('commonTheme', 'Reports'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/report'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-report'
                ],
                'requestedRoute' => ['coupon/report']
            ],
            $couponExchange = [
                'label' => Yii::t('commonTheme', 'Redeem'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/exchange'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-exchange-code'
                ],
                'requestedRoute' => ['coupon/exchange']
            ],
            $earnings = [
                'label' => Yii::t('commonTheme', 'Earnings'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('product/report-sale'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-earnings'
                ],
                'requestedRoute' => ['product/report-sale']
            ],
            $coupons = [
                'label' => Yii::t('commonTheme', 'Products'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-product'
                ],
                'requestedRoute' => ['coupon/index', 'coupon/create', 'coupon/view', 'coupon/update']
            ],
            
            /* Se oculta chat
            $couponChat = [
                'label' => Yii::t('commonTheme', 'Chat'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/chat'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'users'
                ],
                'requestedRoute' => ['coupon/chat']
            ],*/

            /**
             * TODO: Se oculta sección de winkboard hasta que se defina como se implementara en el nuevo flujo
             */
            /*
            $winkboard = [
                'label' => Yii::t('commonTheme', 'Winkboard'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('billboard/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-exchange-code'
                ],
                'controller' => 'billboard'
            ],
            */
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon-sticker/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-sticker'
                ],
                'controller' => 'coupon-sticker'
            ],
        ],
        'reward' => [
            $rewards = [
                'label' => Yii::t('commonTheme', 'Rewards'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-rewards'
                ],
                'requestedRoute' => ['reward/index', 'reward/create', 'reward/view', 'reward/update']
            ],
            $rewardExchange = [
                'label' => Yii::t('commonTheme', 'Exchange'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/exchange'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-exchange-code'
                ],
                'requestedRoute' => ['reward/exchange']
            ],
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward-sticker/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-sticker'
                ],
                'controller' => 'reward-sticker'
            ],
        ],
        'contest' => [
            $contest = [
                'label' => Yii::t('commonTheme', 'Contest'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('contest/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-contest.png",
                'requestedRoute' => ['contest/index']
            ]
        ],
        'ask' => [
            $ask = [
                'label' => Yii::t('commonTheme', 'Surveys'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('ask/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-polls'
                ],
                'requestedRoute' => ['ask/index']
            ],
            $credits = [
                'label' => Yii::t('commonTheme', 'Credits'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-credits'
                ],
                'controller' => 'recharge'
            ],
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('ask-sticker/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'icon-sticker'
                ],
                'controller' => 'ask-sticker'
            ],
        ],
        'b2b-coupon' => [
            $coupons = [
                'label' => Yii::t('commonTheme', 'B2B Coupons'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
                'requestedRoute' => ['b2b-coupon/index', 'b2b-coupon/create', 'b2b-coupon/view', 'b2b-coupon/update']
            ],
            $couponExchange = [
                'label' => Yii::t('commonTheme', 'Redeem'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/exchange'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-barcode.png",
                'requestedRoute' => ['b2b-coupon/exchange']
            ],
            $couponChat = [
                'label' => Yii::t('commonTheme', 'Chat'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/chat'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'users'
                ],
                'requestedRoute' => ['coupon/chat']
            ],
            $couponReports = [
                'label' => Yii::t('commonTheme', 'Reports'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/report'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-reports.png",
                'requestedRoute' => ['b2b-coupon/report']
            ],
        ]
    ];

    $groupAffiliate = [
        [
            'type' => 'submenu',
            'label' => Yii::t('commonTheme', 'General Configuration'),
            'data-toggle' => '.sub-general',
            'id' => 'menuGeneral',
            'items' => [
                $subsidiaries = [
                    'label' => Yii::t('commonTheme', 'Locations'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('subsidiary'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'icon-location'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'subsidiary'
                ],
                $devices = [
                    'label' => Yii::t('commonTheme', 'PIN'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('device'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'icon-pin'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'device'
                ],
                $roles = [
                    'label' => Yii::t('commonTheme', 'Administrators'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('role'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'icon-admin'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'role'
                ],
                $config = [
                    'label' => Yii::t('commonTheme', 'Configuration'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate/'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'icon-config'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'affiliate'
                ],
                /*
                $employees = [
                    'label' => Yii::t('commonTheme', 'Employees'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('employee-affiliate'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'users'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'employee-affiliate'
                ],
                
                $thirParty = [
                    'label' => Yii::t('commonTheme', 'Third Party'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('third-party'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'cubes'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'third-party'
                ]
                */
            ]
        ]
    ];

    $groupAdmin =[
        $users = [
            'label' => Yii::t('commonTheme', 'Users'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-user.png",
        ],
        $affiliates = [
            'label' => Yii::t('commonTheme', 'Affiliates'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png",
        ],
        $cocolect = [
            'label' => Yii::t('commonTheme', 'Cocolect'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('cocolect'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-locations.png",
            'requestedRoute' => ['cocolect']
        ],
        $preregister = [
            'label' => Yii::t('commonTheme', 'Pre-registered'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('preregister'),
            'icon' => [
                'type' => 'font',
                'class' => 'user-plus'
            ]
        ],
        $recruiter = [
            'label' => Yii::t('commonTheme', 'Recruiters'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-recruitment.png",
        ],
        $report = [
            'label' => Yii::t('commonTheme', 'Reports'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('site/report'),
            'icon' => [
                'type' => 'font',
                'class' => 'area-chart'
            ],
        ],
        $deletingImage = [
            'label' => Yii::t('commonTheme', 'Images'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('site/deleting-image'),
            'icon' => [
                'type' => 'font',
                'class' => 'file-image-o'
            ],
        ],
        $massMailing = [
            'label' => Yii::t('commonTheme', 'Mass mailing'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('email-editing-template'),
            'icon' => [
                'type' => 'font',
                'class' => 'envelope-o'
            ],
        ],
        $massMailing = [
            'label' => Yii::t('commonTheme', 'Partner'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('partner'),
            'icon' => [
                'type' => 'font',
                'class' => 'code'
            ],
        ],
        $recomendation = [
            'label' => Yii::t('commonTheme', 'Recomendations'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recomendation'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png",
        ],
    ];
    
    $groupSubsidiary = [
        $couponExchange = [
            'label' => Yii::t('commonTheme', 'Redeem'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/exchange'),
            'icon' => [
                'type' => 'font',
                'class' => 'barcode'
            ]
        ],
        $devices = [
            'label' => Yii::t('commonTheme', 'Devices'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('device'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-device.png",
        ]
    ];
    
    $groupRecruiter = [
        $recruiterCommissions = [
            'label' => Yii::t('commonTheme', 'Commissions'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter/commission'),
            'icon' => [
                'type' => 'font',
                'class' => 'money'
            ],
        ],
        $referred = [
            'label' => Yii::t('commonTheme', 'Referred'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter/referred'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png"
        ]
    ];
    
    $groupParticipant = [
        /* 
         * TODO: Evaluar donde es mejor incluir el módulo de partner.
         * 
         * $myApps = [
         *   'label' => Yii::t('commonTheme', 'Partners'),
         *   'url' => Yii::$app->urlManager->createAbsoluteUrl('partner/index'),
         *   'icon' => [
         *       'type' => 'font',
         *       'class' => 'code'
         *  ],
         * ],
         */
        $profile = [
            'label' => Yii::t('commonTheme', 'Profile'),
            'url' =>  Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
            'icon' => [
                'type' => 'font',
                'class' => 'user'
            ]
        ]
    ];

    switch (Yii::$app->user->identity->user_type) :
        case User::ADMIN:
            $sideMenu = array_merge($sideMenu, $groupAdmin);
            
            if (Yii::$app->getModule('moduleManager') != null && count(Yii::$app->getModule('moduleManager')->getInstalledModules()) > 0) {
                foreach (Yii::$app->getModule('moduleManager')->getInstalledModulesByRoleForThemes('admin') as $format) {
                    $sideMenu[] = $format;
                }
            }   
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['backend/dashboard']);
            break;
        case User::AFFILIATE:
            if (!empty(Yii::$app->session->get('sectionName'))) {
                $sideMenu = array_merge($sideMenu, $solutions[Yii::$app->session->get('sectionName')]);
            }
            $sideMenu = array_merge($sideMenu, $groupAffiliate);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['site/dashboard']);
            break;
        case User::SUBSIDIARY:
            $sideMenu = array_merge($sideMenu, $groupSubsidiary);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['site/dashboard']);
            break;
        case User::PARTICIPANT:
            $sideMenu = array_merge($sideMenu, $groupParticipant);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
            break;
        case User::RECRUITER:
            $sideMenu = array_merge($sideMenu, $groupRecruiter);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
            break;
    endswitch;

endif;
?>

<div id="adminmenuback"></div>
<div class="menu-lateral">
    <a href="<?= $homeRedirect ?>" class="logo-content"><i class="icon-logo"></i></a>
    <ul class="carousel">
        <?php MenuHelper::printMenu($sideMenu)?>
    </ul>
    <div class="clearfix"></div>
</div>
