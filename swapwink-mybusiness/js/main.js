Array.prototype.findInArrayCustom = function (searchStr) {
    var returnArray = false;
    for (i = 0; i < this.length; i++) {
        if (typeof (searchStr) == 'function') {
            if (searchStr.test(this[i])) {
                if (!returnArray) { returnArray = [] }
                returnArray.push(i);
            }
        } else {
            if (this[i] === searchStr) {
                if (!returnArray) { returnArray = [] }
                returnArray.push(i);
            }
        }
    }
    return returnArray;
}

$(document).ready(function () {
    doResponsiveTable();

    $("input[name='menu_open']").on("click", function (e) {
        if ($("input[name='menu_open']").is(':checked')) {
            $("body").addClass("menu-open");
            $("body").append("<div class='menu-overlay'></div>");
            return;
        }

        $("body").removeClass("menu-open");
        $(".menu-overlay").remove();
    });

    $(document).on("click", ".menu-overlay", function (e) {
        $("input[name='menu_open']").trigger("click");
    });

    if ($.cookie("muestraMenu") != null) { /* si la cookie no es vacia */
        var muestra = $.cookie("muestraMenu").split(",");
        var numItems = muestra.length;

        for (i = 0; i < numItems; i++) {
            if (muestra[i] != '') {
                var $id = "#" + muestra[i];
                var dataToggle = $($id).data("toggle");
                $($id).addClass("open");
                $($id).children(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
                $(dataToggle).removeClass("soft-hide");
            }
        }
    }

    $(".header-menu").on("click", function (e) {
        e.preventDefault();

        var id = $(this).prop("id");
        var dataToggle = $(this).data("toggle");
        var muestra = $.cookie("muestraMenu") || "";
        var temp = muestra.split(",");

        if (!$(this).hasClass("open")) {
            temp = openMenuLateral(id, temp, dataToggle);
        } else {
            temp = closeMenuLateral(id, temp, dataToggle);
        }

        /* se guarda la informacion en la cookie */
        $.cookie("muestraMenu", temp.toString(), { path: '/' });
    });
});

function openMenuLateral(id, temp, dataToggle) {
    var $id = "#" + id;
    $(dataToggle).removeClass("soft-hide");
    $($id).addClass("open");
    $($id).children(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    temp.push(id);

    return temp;
}

function closeMenuLateral(id, temp, dataToggle) {
    var $id = "#" + id;
    $(dataToggle).addClass("soft-hide");
    $($id).removeClass("open");
    $($id).children(".fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    temp.splice(temp.findInArrayCustom(id), 1);

    return temp;
}

function doResponsiveTable() {
    $(".nmt").each(function () {
        var nmtTable = $(this);
        var nmtHeadRow = nmtTable.find("thead tr");
        nmtTable.find("tbody tr").each(function () {
            var curRow = $(this);
            for (var i = 0; i < curRow.find("td").length; i++) {
                var rowSelector = "td:eq(" + i + ")";
                var headSelector = "th:eq(" + i + ")";
                curRow.find(rowSelector).attr('data-title', nmtHeadRow.find(headSelector).text());
            }
        });
    });
}
