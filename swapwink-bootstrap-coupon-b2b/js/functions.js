$(document).ready(function(){
	let $body = $('body');
	let notificationClicked = false;
	let notificationPage = 1;
	let notificationPageCount = 100;

	$body.on('click', '.load-notificacitions', function(e){
		let $dataActions = $.parseJSON($(this).attr("data-actions"));

		$.ajax({
			url: $dataActions.load,
			dataType: "html",
			success : function(data) {
				$(".notification-requests .request-list").html(data);
				notificationPage = 1;
			},
			beforeSend: function (returnedData) {
				if (!notificationClicked) {
					notificationClicked = true;
					$(".request-list").html('<i class="fa fa-spinner fa-pulse"></i>');
				}
			}
		});

		$.ajax({
			url: $dataActions.ajaxURL,
			dataType: "json"
		});

		$("#numNotification").html("");
	});

	$('.request-list').on('scroll', function(){
		let diffHeight = $("#preview-notification-requests").height() - $(".request-list").height();

		if ($(".request-list").scrollTop() == diffHeight){
			let $dataActions = $.parseJSON($(".load-notificacitions").attr("data-actions"));

			if (notificationPageCount > notificationPage) {
				notificationPage++;

				$.ajax({
					url: $dataActions.load,
					data: { "page" : notificationPage},
					dataType: "html",
					success : function(data, status, xhr) {
						notificationPageCount = Number(xhr.getResponseHeader('Notifier-Page-Count'));

						if(data.indexOf("empty")  == -1){
							$("#preview-notification-requests").append($(data).filter("#preview-notification-requests").html());
						}

						$("#loading-more").addClass('hidden');
					},
					beforeSend: function (returnedData) {
						$("#loading-more").removeClass('hidden');
					}
				});
			}
		}
	});
});
