<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="footer-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <?= Html::img("https://d224v694zcq7e8.cloudfront.net/themes/coupon/img/logotype-footer.png"); ?>
            </div>
            
            <div class="col-sm-3">
                <h6><?= Yii::t('commonTheme', 'Download our app') ?></h6>
                
                <a class="btn btn-footer" href="https://geo.itunes.apple.com/us/app/swapwink-coupon/id1049457434?mt=8" target="_blank"><i class="fa fa-apple" aria-hidden="true"></i> App store</a>
                <a class="btn btn-footer" href="https://play.google.com/store/apps/details?id=com.swapwink.coupon&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><i class="fa fa-android" aria-hidden="true"></i> Google Play</a>
            </div>

            <div class="col-sm-3">
                <h6><?= Yii::t('commonTheme', 'Follow Us') ?></h6>

                <span class="social-item-footer"><a href="https://www.facebook.com/swapwink" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                <span class="social-item-footer"><a href="https://twitter.com/SwapWink" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                <span class="social-item-footer"><a href="https://instagram.com/swapwink" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></span>
                
            </div>
            
            <div class="col-sm-4">
                <h6><?php echo Yii::t("commonTheme", "Help");?></h6>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li><?= Html::a(Yii::t('commonTheme', 'Privacy'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/privacy')) :  Yii::$app->homeUrl); ?></li>
                            
                            <li><?= Html::a(Yii::t('commonTheme', 'Terms'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/terms')) :  Yii::$app->homeUrl); ?></li>
                            
                            <li><?= Html::a(Yii::t('commonTheme', 'Contact'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/contact')) :  Yii::$app->homeUrl); ?></li>

                            <?php if(isset(Yii::$app->params['linkBlog'])):?>
                                <li><?= Html::a(Yii::t('commonTheme', 'Blog'), Yii::$app->params['linkBlog']); ?></li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="footer-rights">
    <div class="container">
        <p class="text-center">&copy; <?= date('Y') ?> por SwapWink&reg;. <?= Yii::t('commonTheme', 'All rights reserved') ?>.</p>
    </div>
</div>

<img src="https://www.placelocal.com/retarget_pixel.php?cid=589673&uuid=0effa350-8ee6-11e6-9768-002590592b46" width="1" height="1" style="display:none" />


