<?php
use swapwink\themes\AppAssetCouponB2B;

AppAssetCouponB2B::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::$app->sourceLanguage; ?>" lang="<?php echo Yii::$app->language; ?>">

<?= $this->render('//layouts/header') ?>

<body class="body-login">
    <?php $this->beginBody() ?>
	
    <?= $content ?>
    
    <div class="footer-login">
    	&copy; <?php echo date('Y'); ?> por SwapWink&reg;. <?php echo Yii::t('commonTheme', 'All rights reserved'); ?>.
	</div>

    <?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
