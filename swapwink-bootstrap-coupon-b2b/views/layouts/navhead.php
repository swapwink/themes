<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

$location = (isset($this->params['location']))? $this->params['location'] : null;
$languageInfo = User::getInfoToggleLanguage();
$urlChangeLang = Url::toRoute(['/site/changeLanguage', 'lang' => $languageInfo['language']]);

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest):
    $myCoupons = [
        'label' => Yii::t('commonTheme', 'My Coupons'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/myCoupons'),
        'icon' => 'ico-custom ico-coupon-gray',
    ];

    $uploadCoupon = [
        'label' => Yii::t('commonTheme', 'Upload Coupon'),
        'url' => Yii::$app->params['urlMyBusiness'],
        'icon' => 'ico-custom ico-upload'
    ];

    switch (Yii::$app->user->identity->user_type):
        case User::PARTICIPANT:
        case User::AFFILIATE:
            $controlPanelMenu[] = $myCoupons;
            $controlPanelMenu[] = $uploadCoupon;
            break;
    endswitch;

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'ico-custom ico-logout'
    ];

    $numNotifications = Yii::$app->notifier->count();
endif;
?>

<div class="navbar-wrapper navbar">
    <?php
    if ($location != null && Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') : ?>
        <div class="geolocation-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <img src="<?=Yii::$app->params['cdnPathThemesDefault']?>/img/geolocation-icon.png">
                        <p>
                            <?=Yii::t('commonTheme', 'We want to show you the nearby businesses. Are you in <span>{location}</span>? Otherwise', ['location' => $location]);?>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <button class="openModalLocation">
                            <?=Yii::t('commonTheme', 'Select your city');?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="close-geolocation">
                <span class="ico ico-check"></span>
            </div>
        </div>
    <?php
    endif;?>
    
    <div class="download-app-header">
        <div class="container">
            <div class="pull-right">
                <section>
                    <?php if(isset(Yii::$app->params['linkBlog'])):?>
                        <a target="_blank" href="<?php echo Yii::$app->params['linkBlog']?>" class="link">Blog</a>
                    <?php endif;?>

                    <?=Yii::t('commonTheme', 'Download our app')?>
                    <a target="_blank" href="<?= Yii::$app->params['appleAppLink']?>">
                        <i class="fa fa-apple"></i>
                    </a>
                    <a target="_blank" href="<?= Yii::$app->params['androidAppLink']?>">
                        <i class="fa fa-android"></i>
                    </a>
                    <div class="dropdown">
                        <a id="dLabel" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="<?=User::getInfoLanguage()['icon']?>"></div>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>
                                <a href="<?=$urlChangeLang?>">
                                    <div class="<?=$languageInfo['icon']?>"></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="navbar-head">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <a id="home_url" href="<?= Yii::$app->homeUrl ?>">
                        <?= Html::img("https://d224v694zcq7e8.cloudfront.net/themes/coupon/img/logotype.png", ['class' => 'navbar-brand', 'alt' => Yii::$app->name]); ?>
                    </a>
                </div>

                <div class="col-md-4 hidden-xs hidden-sm">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'class' => 'search-form',
                        'method' => 'post',
                        'action' => Url::to(['site/index'])
                    ]);
                    ?>

                    <input type="hidden" name="slug" value="<?= (Yii::$app->request->get('slug')) ? Yii::$app->request->get('slug') : "" ?>">

                    <div class="coupon-search-content<?= (!empty($this->params['hideSearchCoupon']) && $this->params['hideSearchCoupon']) ? " hidden" : "" ?>">
                        <?= Html::textInput('search_string', '', ['placeholder' => Yii::t('commonTheme', 'Search'), 'maxlength' => 40]); ?> <i class="fa fa-search"></i>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="row">
                        <?php
                        if (!\Yii::$app->user->isGuest): ?>

                            <?php
                            if (Yii::$app->session->get('b2b_affiliate_model')) :?>
                                <div class="control-panel pull-right">
                                    <div class="dropdown dropdown-user-menu">
                                        <div class="dropdown-toggle user-avatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <div class="user-info">
                                                <figure>
                                                    <?= Html::img(Yii::$app->session->get('b2b_affiliate_model')->urlLogo); ?>
                                                </figure>
                                                <i class="fa fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <?= Html::a('<span class="ico-user ico-custom"></span>' . Yii::t('base', 'Change affiliate'), '#', ['class' => '', 'data-toggle' => 'modal', 'data-target' => '#modalAffiliateSelector']) ?>
                                            </li>
                                            <?php
                                            foreach ($controlPanelMenu as $item) :
                                                if (!empty($item['url'])) :
                                                    $linkLabel = '<span class="' . $item['icon'] . '"></span>' . $item['label'];
                                                    echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                                                else:
                                                    echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                                                endif;
                                            endforeach;
                                            ?>
                                        </ul>
                                    </div>
                                </div><!--.control-panel-->

                                <div class="control-panel pull-right">
                                    <div class="dropdown dropdown-user-menu-notification">
                                        <div class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <div class="load-notificacitions" data-actions='{"load":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/preview") ?>", "ajaxURL":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/check-as-read") ?>"}'>
                                                <span class="fa fa-bell"></span>
                                                <span id="numNotification" class="label"><?= ($numNotifications > 0) ? $numNotifications : '' ?></span>
                                            </div>
                                        </div>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <div class="box-messages notification-requests">
                                                    <div class="request-list"></div>
                                                    <i id="loading-more" class="fa fa-spinner fa-pulse hidden"></i>
                                                </div><!--.notification-requests-->
                                            </li>
                                            <li>
                                                <div class='view-all'><?= Html::a(Yii::t('commonTheme', 'view all'), Yii::$app->urlManager->createUrl('notifier/index')) ?></div>
                                            </li>
                                        </div>
                                    </div>
                                </div><!--.control-panel-->
                            <?php
                            endif;?>

                        <?php
                        elseif (!empty(Yii::$app->session->get('preregister'))) :?>

                            <div class="control-guest pull-right">
                                <a href="#" class="a-guest" style="text-align: right">
                                    <?= Yii::$app->session->get('preregister')->first_name ?>
                                    <?= Yii::$app->session->get('preregister')->last_name ?>
                                    <br>
                                    <?= Yii::$app->session->get('preregister')->email ?>
                                </a>
                            </div>

                        <?php
                        else :?>

                            <div class="control-guest pull-right">
                                <a href="<?= Yii::$app->homeUrl ?>" class="a-guest">
                                    <?= Yii::t('commonTheme', 'Login / Register'); ?>
                                </a>
                            </div><!--.control-panel-->

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!--.navbar-head-->
</div><!--.navbar-wrapper-->

<?= $this->render('//site/_affiliate_selector') ?>

<?php
$this->registerJs('
    $("body").ready(function(){
        $(".close-geolocation span").on("click", function(){
            $(".geolocation-header").slideUp();
        });
    });
');
?>