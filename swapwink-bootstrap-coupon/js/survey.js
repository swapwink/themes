/**
 * Oculta los campos survey al seleccionar la encuesta de servicio.
 */

$('#SurveyModel_survey_type').on('change',function(){
    
    var typeSurvey = $(this).val();
    var general = 0;
    var servicio = 1;
    
    if(typeSurvey == general){
        
        $('#_dropdowns').fadeOut();
        
    }else if(typeSurvey == servicio){
        
        $('#_dropdowns').fadeIn();
        
    }
});


/**
 * Se utiliza en el listado de encuestas para cambiar de estatus
 * las encuestas.
 */
$(document).on('click', '.change_status', function(e) {
    
    e.preventDefault();
    var newStatus = $(this).attr('data-status');    
    var surveyId = $(this).parent().parent().attr('data-id');
    var ajaxLink = $(this).parent().parent().attr('data-link');
    
                $.ajax({
			type: 'POST',
			url: ajaxLink,
			dataType: 'json',
                        data:{ status:newStatus, id:surveyId },
			success: function(data){
                            
                            if(data.success == 'false'){
                                
                                $('.errorMessage').html('');
                                $('.errorMessage').css('display','none');
                                $('#survey_' + surveyId + ' .errorMessage').css('display','block');
                                
                                $.each(data.errors, function(index, value) {
                                   
                                    $('#survey_' + surveyId + ' .errorMessage').append(value + "<br>").hide().fadeIn();

                                });
                                
                            }
                            else{
                                
                                $('#survey_' + surveyId + ' .buttons').html("").append(data.buttons).hide().fadeIn();
                                $('#survey_' + surveyId + ' .status').html("").append(data.newStatus).hide().fadeIn();

                                
                            }
                            
			}
                        
		});
});