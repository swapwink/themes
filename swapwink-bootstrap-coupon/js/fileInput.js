
var fileExtension = "";

$(':submit').click(function(e) {
    var message = "";

    if (fileExtension !== "") {

        showMessage("Subiendo la imagen, por favor espere...");

    }

});

/**
 *
 * Recibe la información del archivo subido. 
 */
function getFilename(element) {

    var file = $("#" + element.id)[0].files[0];
    var fileName = file.name;
    fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
    var fileSize = file.size;
    var fileType = file.type;

    if (isImage(fileExtension)) {

        showMessage("(" + fileName + "), (" + fileSize + ") bytes.");

    } else {

        showMessage("Archivo no permitido, suba alguna imagen con extension .png o .jpg.");
        $(form_id)[0].files[0] = null;
        fileExtension = "";

    }
}


/**
 * Muestra la informacion de la imagen.
 */
function showMessage(message) {

    $(".file_information").html("").show();
    $(".file_information").html("<h5>" + message + "</h5>");

}

/**
 * Comprueba que sea una imagen valida.
 */
function isImage(extension)
{
    switch (extension.toLowerCase())
    {
        case 'jpg':
        case 'png':
            return true;
            break;
        default:
            return false;
            
            break;
    }
}


