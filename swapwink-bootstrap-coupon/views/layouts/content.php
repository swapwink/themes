<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="content container">
    <?php echo $content; ?>
</div>

<?php $this->endContent();?>
