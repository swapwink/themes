<?php

use common\widgets\Alert;
use swapwink\themes\AppAssetCoupon;
use yii\helpers\Html;
use yii\helpers\Url;

AppAssetCoupon::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage ?>" lang="<?= Yii::$app->language ?>">

    <?= $this->render('//layouts/header') ?>

    <?php if (in_array($this->context->action->id, array("login"))) : ?>

        <body class="body-login">
            <?php $this->beginBody() ?>

            <?= $content ?>

            <div class="footer-login">
                &copy; <?= date('Y') ?> por SwapWink&reg;. <?= Yii::t('commonTheme', 'All rights reserved') ?>.
                <?= Html::a(Yii::t('commonTheme', 'Privacy'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/privacy')) : Yii::$app->homeUrl, ['style' => 'color:#fff']) ?>.
                <?= Html::a(Yii::t('commonTheme', 'Terms'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/terms')) : Yii::$app->homeUrl, ['style' => 'color:#fff']) ?>
            </div>

            <?php $this->endBody() ?>
        </body>

    <?php else: ?>
        <body>
            <?php $this->beginBody() ?>
            <?= $this->render('//layouts/navhead') ?>
            <?= Alert::widget() ?>
            <?= $content; ?>
            <?= $this->render('//layouts/footer') ?>
            <?php $this->endBody() ?>
        </body>
    <?php endif; ?>

</html>

<?php $this->endPage() ?>
