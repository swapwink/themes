<?php
use common\widgets\Alert;
use swapwink\themes\AppAssetCoupon;
AppAssetCoupon::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage ?>" lang="<?= Yii::$app->language ?>">
    <?= $this->render('//layouts/header') ?>
    <body class="custom-page">
        <?php $this->beginBody() ?>
        <?= $this->render('//layouts/navhead') ?>
        <?= Alert::widget() ?>
        <?= $content; ?>
        <?= $this->render('//layouts/footer') ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
