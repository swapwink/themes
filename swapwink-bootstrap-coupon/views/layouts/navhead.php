<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

$location = (isset($this->params['location']))? $this->params['location'] : null;
$languageInfo = User::getInfoToggleLanguage();
$urlChangeLang = Url::toRoute(['/site/changeLanguage', 'lang' => $languageInfo['language']]);

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest):

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Profile'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
        'icon' => 'ico-custom ico-user'
    ];

    $myCoupons = [
        'label' => Yii::t('commonTheme', 'My Coupons'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/myCoupons'),
        'icon' => 'ico-custom ico-coupon-gray',
    ];

    $myProducts = [
        'label' => Yii::t('commonTheme', 'My Products'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/myProducts'),
        'icon' => 'ico-custom ico-coupon-gray',
    ];

    $uploadCoupon = [
        'label' => Yii::t('commonTheme', 'Upload product'),
        'url' => Yii::$app->params['urlMyBusiness'],
        'icon' => 'ico-custom ico-upload'
    ];

    switch (Yii::$app->user->identity->user_type):
        case User::PARTICIPANT:
        case User::AFFILIATE:
            $controlPanelMenu[] = $myCoupons;
            $controlPanelMenu[] = $myProducts;
            $controlPanelMenu[] = $uploadCoupon;
            break;
    endswitch;

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'ico-custom ico-logout'
    ];

    $numNotifications = Yii::$app->notifier->count();
endif;
?>

<?php
$this->registerCss('
    .download-app-header .link{
        color: #fff;
        border-bottom: 1px solid #fff;
        margin: 0 30px;
    }
    .download-app-header .link:hover{
        color: #fff;
    }
    
    .geolocation-header{
        background-color: #ff005a;
        position:relative;
    }
    
    .geolocation-header div.container p {
        font-size:14px;
        color: #ffffff;
        font-weight:300;
        text-align:center;
        margin:0 auto;
        padding-top:20px;
        padding-left:20px;
        clear:none;
        float:left;
    }
    
    .geolocation-header div.container img{
        clear:none;
        float:left;
        margin: 0 auto;
    }
    
    .geolocation-header div.container p span{
        text-decoration: underline;
        font-style: italic;
    }
    
    .geolocation-header div.container button{
        border: 1px #ffffff solid;
        background: transparent !important;
        color:#ffffff;
        padding:5px 10px;
        margin-top:15px;
        width:100%;
        font-size:12px;
    }
    
    .geolocation-header div.container div.row{
        padding: 10px 0px;
        vertical-align:middle;
    }
    .geolocation-header div.container div.row > div{
        padding:0;
    }
    
    .close-geolocation{
        position:absolute;
        right:25px;
        top:10px;
    }
    
    .close-geolocation span{
        color: #ffffff;
        cursor: pointer;
        border: 1px solid #fff;
        border-radius: 100%;
        font-size: 18px;
        margin-top: 15px;
        display: block;
        width: 30px;
        height: 30px;
        text-align: center;
        line-height: 30px;
    }
');

$this->registerJs('
    $("body").ready(function(){
        $(".close-geolocation span").on("click", function(){
            $(".geolocation-header").slideUp();
        });
    });
');
?>



<div class="navbar-wrapper navbar">
    <?php
    if ($location != null && Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') :
        ?>
        <div class="geolocation-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <img src="<?=Yii::$app->params['cdnPathThemesDefault']?>/img/geolocation-icon.png">
                        <p>
                            <?=Yii::t('commonTheme', 'We want to show you the nearby businesses. Are you in <span>{location}</span>? Otherwise', ['location' => $location]);?>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <button class="openModalLocation">
                            <?=Yii::t('commonTheme', 'Select your city');?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="close-geolocation">
                <span class="ico ico-check"></span>
            </div>
        </div>
        <?php
    endif;
    ?>
    <div class="download-app-header">
        <div class="container">
            <div class="pull-right">
                <section>
                    <?php if(isset(Yii::$app->params['linkBlog'])):?>
                        <a target="_blank" href="<?php echo Yii::$app->params['linkBlog']?>" class="link">Blog</a>
                    <?php endif;?>

                    <?=Yii::t('commonTheme', 'Download our app')?>
                    <a target="_blank" href="<?= Yii::$app->params['appleAppLink']?>">
                        <i class="fa fa-apple"></i>
                    </a>
                    <a target="_blank" href="<?= Yii::$app->params['androidAppLink']?>">
                        <i class="fa fa-android"></i>
                    </a>
                    <div class="dropdown">
                        <a id="dLabel" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="<?=User::getInfoLanguage()['icon']?>"></div>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>
                                <a href="<?=$urlChangeLang?>">
                                    <div class="<?=$languageInfo['icon']?>"></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="navbar-head">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <a id="home_url" href="<?= Yii::$app->homeUrl ?>">
                        <?= Html::img("https://d224v694zcq7e8.cloudfront.net/themes/coupon/img/logotype.png", ['class' => 'navbar-brand', 'alt' => Yii::$app->name]); ?>
                    </a>
                </div>

                <div class="col-md-4 hidden-xs hidden-sm">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'class' => 'search-form',
                        'method' => 'post',
                        'action' => Url::to(['site/index'])
                    ]);
                    ?>

                    <input type="hidden" name="slug" value="<?= (Yii::$app->request->get('slug')) ? Yii::$app->request->get('slug') : "" ?>">

                    <div class="coupon-search-content<?= (!empty($this->params['hideSearchCoupon']) && $this->params['hideSearchCoupon']) ? " hidden" : "" ?>">
                        <?= Html::textInput('search_string', '', ['placeholder' => Yii::t('commonTheme', 'Search'), 'maxlength' => 40]); ?> <i class="fa fa-search"></i>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="row">
                        <?php if (!\Yii::$app->user->isGuest): ?>
                            <div class="control-panel pull-right">
                                <div class="user-info button_switch">
                                    <div class="user-avatar pull-left">
                                        <figure>
                                            <?php echo Html::img(Yii::$app->user->identity->urlAvatar, ['id' => 'avatar_profile', 'class' => 'user-avatar']); ?>
                                        </figure>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div><!--.user-info-->

                                <div class="drop-list">
                                    <span class="arrow"></span>
                                    <ul>
                                        <li class="user-list-winks">
                                            <?php
                                            $winks = 0;
                                            if (Yii::$app->has('rewardComponent') && isset(\Yii::$app->user->identity) && isset(Yii::$app->rewardComponent)) {
                                                $response = Yii::$app->rewardComponent->getWinks(\Yii::$app->user->identity->swap_id);
                                                if ($response && Yii::$app->response->isOk) {
                                                    $winks = $response->amount;
                                                }
                                            }
                                            ?>
                                            <span class="fake-link"><span class="ico-custom ico-star"></span> <?= $winks ?> Winks</span>
                                        </li>

                                        <?php
                                        foreach ($controlPanelMenu as $item) :
                                            if (!empty($item['url'])) :
                                                $linkLabel = '<span class="' . $item['icon'] . '"></span>' . $item['label'];
                                                echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                                            else:
                                                echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                                            endif;
                                        endforeach;
                                        ?>
                                    </ul>
                                </div><!--.drop-list-->
                            </div><!--.control-panel-->

                            <div class="container-messages-notifications">
                                <div class="notification-header container-messages">
                                    <a class="notification-head navbar-buttons" data-actions='{"load":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/preview") ?>", "ajaxURL":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/check-as-read") ?>"}'>
                                        <span class="fa fa-bell"></span>
                                        <span id="numNotification" class="label"><?= ($numNotifications > 0) ? $numNotifications : '' ?></span>
                                    </a>
                                    <div class="box-messages notification-requests">
                                        <span class="arrow"></span>
                                        <div class="request-list"></div>
                                        <i id="loading-more" class="fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom hidden" style="margin-left:auto; margin-right:auto; display:block;"></i>
                                        <div class='view-all'><?= Html::a(Yii::t('commonTheme', 'view all'), Yii::$app->urlManager->createUrl('notifier/index')) ?></div>
                                    </div><!--.notification-requests-->
                                </div><!--.notification-header-->
                            </div><!--.container-messages-notifications-->
                            <?php
                        else :
                            if (!empty(Yii::$app->session->get('preregister'))) :
                                ?>
                                <div class="control-guest pull-right">
                                    <a href="#" class="a-guest" style="text-align: right">
                                        <?= Yii::$app->session->get('preregister')->first_name ?>
                                        <?= Yii::$app->session->get('preregister')->last_name ?>
                                        <br>
                                        <?= Yii::$app->session->get('preregister')->email ?>
                                    </a>
                                </div>
                            <?php else :
                                ?>
                                <div class="control-guest pull-right">
                                    <a href="<?= Yii::$app->homeUrl ?>" class="a-guest">
                                        <?= Yii::t('commonTheme', 'Login / Register'); ?>
                                    </a>
                                </div><!--.control-panel-->
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!--.navbar-head-->
</div><!--.navbar-wrapper-->
