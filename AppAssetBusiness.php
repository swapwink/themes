<?php

namespace swapwink\themes;

use yii\web\AssetBundle;

class AppAssetBusiness extends AssetBundle
{
    
    public $sourcePath;
    public $basePath;
    public $baseUrl;
    public $css = [
        'css/main.less',
        '//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css'
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/bootstrap-switch.min.js',
        'js/jquery-ui-1.10.4.custom.min.js',
        'js/select2.min.js',
        'js/jquery.tipsy.min.js',
        'js/iframeResizer.js',
        'js/bootbox.min.js',
        'js/functions.js',
        '//cdn.jsdelivr.net/momentjs/latest/moment.min.js',
        '//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $cssOptions = ['position' => \yii\web\View::POS_HEAD];

    public function init()
    {
        $this->sourcePath = !YII_DEBUG ? '@vendor/swapwink/themes/business' : null; //si esta definida realiza assetManager->publish()
        $this->basePath = !YII_DEBUG ? null : '@vendor/swapwink/themes/business';
        $this->baseUrl = !YII_DEBUG ? null : '/vendor/swapwink/themes/business/';

        parent::init();

        // restablecer BootstrapAsset para que no cargue sus archivos js que causan conflicto
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'js' => []
        ];
    }
}
