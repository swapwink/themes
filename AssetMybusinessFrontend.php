<?php

namespace swapwink\themes;

use yii\web\AssetBundle;

class AssetMybusinessFrontend extends AssetBundle
{

    public $sourcePath;
    public $basePath;
    public $baseUrl;
    public $css = [
        'css/main.less',
        'css/swapwink-icons.css',
        '//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
    ];
    public $js = [
        'js/plugins.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $cssOptions = ['position' => \yii\web\View::POS_HEAD];

    public function init()
    {
        $base = 'vendor/swapwink/themes/swapwink-mybusiness';
        $this->sourcePath = !YII_DEBUG ? '@' . $base : null;
        $this->basePath = !YII_DEBUG ? null : '@' . $base;
        $this->baseUrl = !YII_DEBUG ? null : '/' . $base . '/';

        parent::init();
    }
}
