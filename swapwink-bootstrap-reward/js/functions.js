$(document).ready(function(){
    var body	= $('body'),
        content		= $('.content');

    /* *****					  		*****************************************************				*/
    /* #####	DROPDOWN LIST 	 		#####################################################				*/
    /* *****	 						*****************************************************				*/

    body.on('click', '.button_switch', function(e){
        e.stopPropagation();
        e.preventDefault();

        var this_element	= $(this);
        var drop			= this_element.parent().find('.drop-list');

        if(this_element.hasClass('active')){
            this_element.removeClass('active');
        }else{
            var anotherActive     = $('.button_switch.active');
            var dropAnotherActive = anotherActive.parent().find('.drop-list');

            if(anotherActive.length){
                anotherActive.removeClass('active');
                dropAnotherActive.fadeToggle('fast');
            }

            $(".box-messages").fadeOut("fast");
            this_element.addClass('active');
        }

        drop.fadeToggle('fast');
    });

    /* Notificaciones */

    body.on('click', '#notifications-icon', function(){
        var globito = $(this).find('span');

        if(globito.length){
            globito.remove();
            // Update de notificaciones como leídas
        }
    });

    //Cerrar el cuadro de búsqueda cuando se haga click fuera

    body.click(function() {
        var button_switch	= $(".button_switch.active"),
            drop				= button_switch.parent().find('.drop-list');

        button_switch.removeClass("toggled");
        drop.fadeToggle('fast');
        button_switch.removeClass('active');
    });

    /* *****	FUNCIÓN GLOBAL	  		*****************************************************				*/
    /* #####	PARA CAMBIAR CLASE 		#####################################################				*/
    /* *****	ACTIVE EN CLICK	 		*****************************************************				*/

    body.on('click', '.switch_option', function(event){
        var this_element	= $(this);
        var brother_active	= this_element.parent().find('.active');

        brother_active.removeClass('active');
        this_element.addClass('active');

    });

    /* *****					  		*****************************************************				*/
    /* #####	PANEL INTERACTIONS 	 	#####################################################				*/
    /* *****	 						*****************************************************				*/

    $(window).bind('resize', function(event){
        updatePanelHeight();

        var popupDialog = $( ".popup" );

        if(popupDialog.length){
            popupDialog.dialog( "option", "position", { my: "center", at: "center", of: window } );
        }
    });


    function updatePanelPosition(callback){
        var panels = $('.proces-panel');

        if(panels.length) {
            panels.each(function(){
                var this_element = $(this);
                if($('.navbar-fixed-top').length){
                    this_element.css('position','fixed')
                        .stop(false, true).animate({ top: '67px', height: '86%' },0);
                }else{
                    this_element.stop(false, true).animate({top: '125px'}, 0, function(){
                        this_element.css('position','');

                        if(callback !== undefined){
                            callback();
                        }
                    });
                }
            });
        }
    }

    function updatePanelHeight(callback){
        var panels = $('.proces-panel');

        if( panels.length){
            panels.each(function(){
                var this_element = $(this);
                var topPosition = this_element.css('top');
                topPosition = topPosition.replace('px','');
                topPosition = topPosition.replace('-','');

                this_element.stop(false, true).animate(
                    { height: (($(window).height()-topPosition)-5)+'px' },
                    400,
                    function(){
                        if(callback !== undefined){
                            callback();
                        }
                    }
                );
            });
        }
    }

    /* *****	PANEL DINAMICO,	  		*****************************************************				*/
    /* #####	CARGA DE VISTAS EN 		#####################################################				*/
    /* *****	PANEL VÍA AJAX	 		*****************************************************				*/

    var last_view;

    body.on('click', '.panel-trigger', function(e){
        e.preventDefault();
        openPanel($(this));
    });

    var panelAnimation = false;

    function openPanel(this_element, custom_view){
        if(custom_view === undefined){
            var data_view = this_element.attr('data-view');

            if(data_view === undefined){
                data_view = this_element.attr('href');
            }

        }else{
            var data_view	= custom_view;
        }

        var panel			= $('.active_panel');
        var sub_panel		= this_element.attr('data-subpanel');

        if (data_view !== undefined && !panelAnimation) {
            body.css('overflow-x', 'hidden');
            panelAnimation = true;
            var animate = 300;

            //Hidde all tooltips
            this_element.mouseout();
            //Hidde all choosens
            if($('.select2-offscreen').length){
                $('.select2-offscreen').select2('close');
            }

            if($('.proces-panel').length){
                if( sub_panel == 'true' && sub_panel !== undefined ){
                    //If there are more than two panels, remove active
                    if( $('.proces-panel').length >= 2 ){
                        panel.remove();
                        animate = 0;
                    }else{
                        panel.removeClass('active_panel');
                    }
                }else{
                    //Remove all if not subpanel
                    $('.proces-panel').remove();
                    animate = 0;
                }
            }

            // Set de last view after removing panels logic
            last_view = data_view;

            content.after($('<div>').addClass('proces-panel').addClass('active_panel'));

            updatePanelPosition(function(){ });

            updatePanelHeight();

            $('.active_panel').css('right', '-35%')
                .css('opacity', 0)
                .animate({right: '2%', opacity: 1}, animate, function(){
                    panelAnimation = false;
                    body.css('overflow-x','auto');
                });

            $('.active_panel').prepend('<div original-title="Cerrar" class="mws-tooltip-s close_panel fa fa-times-circle"></div>');

            $.ajax({
                url: data_view,
                type: 'POST',
                data: { ajaxRequest: true },
                dataType: 'html',
                success: function(data){
                    $('.active_panel').append(data);
                    initProcesComponents();
                },
                error: function(data){
                    $('.active_panel').append(data);
                },
                complete: function(){
                }
            });
        }
    }

    body.on('click', '.close_panel, .proces-panel .cancel-button', function(event) {
        event.preventDefault();
        closePanel( $(this) );
    });

    body.on('click', '.content .cancel-button', function(event) {
        event.preventDefault();
        window.history.back();
    });

    $(document).keyup(function(e){
        if (e.keyCode == 27){ // Escape
            closePanel();
        }
    });

    //Close panel
    function closePanel(this_element){
        var current_panel;

        if( this_element === undefined ){
            current_panel = $('.active_panel');
        }else{
            current_panel = this_element.parents('.active_panel');
        }

        if(current_panel.length){
            if(!panelAnimation){
                panelAnimation = true;

                if($('.tipsy')){
                    $('.tipsy').remove();
                }

                body.css('overflow-x', 'hidden');

                current_panel.animate({right: '-30%'}, {
                    duration: 300,
                    complete: function(){
                        body.css('overflow-x', 'auto');

                        panelAnimation = false;

                        $(this).remove();

                        //Si no queda ningun panel abierto
                        if( $('.proces-panel').length == 1 ){
                            $('.proces-panel').addClass('active_panel');
                        }

                        if($('.tipsy').length){
                            $('.tipsy').remove();
                        }
                    }
                });
            }
        }
    }

    /* *****	POPUP DINAMICO,	  		*****************************************************				*/
    /* #####	CARGA DE VISTAS EN 		#####################################################				*/
    /* *****	POPUP VÍA AJAX	 		*****************************************************				*/

    var last_view;

    body.on('click', '.popup-trigger', function(e){
        e.preventDefault();
        openPopup($(this));
    });

    var popupAnimation = false;

    function openPopup(this_element, type, custom_data){
        var data_view = this_element.attr('data-view');
        var data_post = "ajaxRequest=" + encodeURIComponent('true');
        var popup     = $('.popup');

        if(type == 'manual'){
            if(custom_data.data_view !== undefined){
                data_view = custom_data.data_view;
            }

            if(custom_data.data_post !== undefined){
                data_post = custom_data.data_post;
                data_post.ajaxRequest = true;
            }
        }

        if (data_view !== undefined){
            //Hidde all tooltips
            this_element.mouseout();

            //Hidde all choosens
            if($('.select2-offscreen').length){
                $('.select2-offscreen').select2('close');
            }

            if(popup.length){
                popup.dialog("destroy").remove();
            }

            content.after(
                $('<div>').addClass('popup')
            );

            $('.popup').prepend('<div original-title="Cerrar" class="mws-tooltip-s close_popup"></div>');

            popup = $('.popup');

            $.ajax({
                url     : data_view,
                type    : 'POST',
                data    : data_post,
                dataType: 'html',
                success : function(data){
                    popup.append(data);
                    popup.dialog({
                        show       : { effect: "fadeIn", duration: 400 },
                        width      : 700,
                        draggable  : false,
                        resizable  : false
                    });

                    body.append('<div class="dialog-background"></div>').fadeOut(0).fadeIn('fast');

                    popup.on("dialogclose", function(event, ui){
                        closePopup();
                    });

                    initProcesComponents();
                },
                error: function(data){
                    popup.append(data);
                    popup.dialog();
                },
                complete: function(){

                }
            });
        }
    }

    body.on('click', '.close_popup', function(){
        closePopup();
    });

    $(document).keyup(function(e){
        if (e.keyCode == 27){ // Escape
            closePopup();
        }
    });

    //Close panel
    function closePopup(){
        $('.popup').dialog("destroy").remove();
        $('.tipsy').remove();
        $('.dialog-background').fadeOut('fast', function(){
            $(this).remove();
        });
    }

    function dialogConfirm(config){
        var elementString = '<div class="panel-options"><div>';
        elementString     += '<h2>'+config.title+'</h2>';
        elementString     += '</div></div>';

        elementString     += '><div class="panel-content">';
        elementString     += '<div class="cols" data-cols="4">';
        elementString     += '<p>'+config.text+'</p>';
        elementString     += '</div></div>';

        content.append('<div class="message_element popup">'+elementString+'</div>');

        var popup = $('.popup');

        body.append('<div class="dialog-background"></div>').fadeOut(0).fadeIn('fast');
        popup.prepend('<div original-title="Cerrar" class="mws-tooltip-s close_popup"></div>');

        popup.dialog({
            show       : { effect: "fadeIn", duration: 400 },
            width      : 700,
            draggable  : false,
            resizable  : false,
            buttons: [
                {
                    text: "Aceptar",
                    "class": 'acept-button redbtn',
                    click: function(){
                        closePopup();
                        config.callbackTrue(popup);
                    }
                },
                {
                    text: "Cancelar",
                    "class": 'cancel-button whitebtn',
                    click: function(){
                        closePopup();
                        config.callbackFalse(popup);
                    }
                }
            ],
            create: function(){
                var buttonsContainer = $('.ui-dialog-buttonpane');
                var wrapperSection   = '<div class="panel-section"></div>';
                var wrapperCols      = '<div class="cols" data-cols="4"></div>';

                buttonsContainer.appendTo( popup.find('.panel-content') )
                    .wrap(wrapperCols)
                    .wrap(wrapperSection);

                var wrapperButtons = '<div class="cols" data-cols="2"></div>';
                var cancelButton   = buttonsContainer.find('.cancel-button');
                var aceptButton    = buttonsContainer.find('.acept-button');

                aceptButton.unwrap().unwrap().css('width', '100%').wrap(wrapperButtons);
                cancelButton.css('width', '100%').wrap(wrapperButtons);

                config.callbackReady(popup)
            }
        });
    }

    /* *****	TOOLTIPS EN DIFERENTES  	*****************************************************				*/
    /* #####	DIRECCIONES VISTAS			#####################################################				*/
    /* *****	SELCTOR DE CLASES 			*****************************************************				*/

    var someTooltip = $('[class*="mws-tooltip"]');

    if(someTooltip.length){
        var gravity = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];

        for(var i in gravity){
            $(".mws-tooltip-"+gravity[i]).tipsy({
                gravity: gravity[i],
                live: true,
                delayIn: 250,
                offset: 7,
                fade: true,
                opacity: .8
            });
        }

        body.on('click', '.tipsy', function(event){
            $(this).remove();
        });
    }

    /* *****	  							*****************************************************				*/
    /* #####	INPUTS TIPO DATE			#####################################################				*/
    /* *****								*****************************************************				*/

    function createInputDate(){
        var inputsDate = $(".datepicker");

        if(inputsDate.length){
            inputsDate.each(function(){
                var this_element = $(this);

                this_element.datepicker({
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                    dateFormat: 'yy-mm-dd',
                    showOtherMonths:true,
                    beforeShow: function(input, obj){
                        this_element.addClass('data_picker_active');
                    },
                    onClose: function(input, obj){
                        this_element.removeClass('data_picker_active');
                    }
                });

                var datePickerId		= this_element.attr('id');
                var datePickerTrigger	= this_element.siblings('.datepicker_trigger');

                datePickerTrigger.attr('datepicker-trigger', datePickerId);

                body.on('click', '[datepicker-trigger='+datePickerId+']', function(e){

                    if (this_element.datepicker('widget').is(':hidden')){
                        this_element.datepicker("show");
                    }else{
                        this_element.mouseout();
                    }
                });
            });
        }
    }

    /* *****							*****************************************************				*/
    /* #####	CHOOSEN WITH AJAX		#####################################################				*/
    /* *****				 			*****************************************************				*/

    function format(item, container){
        if(item.id == 'create'){
            return '<a href="#" class="choosen_create panel-trigger" data-subpanel="true">'+item.text+'</a>';
        }else{
            return '<p class="choosen_item">'+item.text+'</p>';
        }
    }

    var selectsCount = 0;

    function createAjaxChoosen(){

        // Normal select choosen
        var selectChoosen = $('select');

        selectChoosen.each(function(){
            var this_element    = $(this);
            var data_new        = this_element.attr('data-new');
            var placeholder 	= this_element.attr('placeholder');

            var thisChoosenConfig = {
                allowClear         : true,
                formatResult       : format,
                formatSelection    : format,
                formatInputTooShort: 'Ingrese palabras clave...',
                formatLoadMore     : 'Cargando resultados...',
            };

            if(data_new){
                var customData     = {};
                var optionsElement = this_element.find('option');

                optionsElement.each(function(index, el){
                    data[index] = {id: el.value, text: el.text};
                });

                thisChoosenConfig.data = customData;
            }

            if(placeholder !== undefined && thisChoosenConfig.allowClear){
                this_element.prepend('<option></option>');

                if(!this_element.find('option[selected]').length){
                    this_element.prop("selectedIndex", 0);
                }
            }

            if (typeof this_element.select2 == 'function'){
                this_element.select2(thisChoosenConfig);
            }

            // createAddNewButtton(this_element);
        });

        //Ajax source choosens
        var choosens = $('.ajax_choosen');

        choosens.each(function(){
            selectsCount++;

            var this_element	= $(this);

            if(!this_element.hasClass('select2-offscreen')){
                var data_view        = this_element.attr('data-view');
                var data_new         = this_element.attr('data-new');
                var elementSource    = this_element.attr('element-source');
                var dataViewNoSource = this_element.attr('dataview-nosource');
                var dataNewNoSource  = this_element.attr('datanew-nosource');
                var placeholder      = this_element.attr('placeholder');
                var name             = this_element.attr('name');

                if(placeholder === undefined){
                    placeholder = 'Escriba para iniciar la búsqueda...';
                }

                if( data_view !== undefined){

                    if(elementSource !== undefined){
                        var formParent	= this_element.parents('form');
                        elementSource	= formParent.find('[name="'+elementSource+'"]');

                        //If wasn't specified "not refreshing the data-new property"
                        if(dataNewNoSource != 'false'){
                            data_new		= this_element.attr('data-new')+elementSource.val();
                        }

                        //If wasn't specified "not refreshing the data-view property"
                        if(dataViewNoSource != 'false'){
                            data_view		= this_element.attr('data-view')+elementSource.val();
                        }
                    }

                    if(data_new !== undefined){
                        this_element.attr('data-subpanel', 'true');
                    }
                    var selectId = 'proces-select2-'+selectsCount;

                    this_element.attr('select-id', selectId);

                    var config = {
                        allowClear         : true,
                        formatResult       : format,
                        formatSelection    : format,
                        dropdownCssClass   : selectId,
                        disabled           : false,
                        formatInputTooShort: 'Ingrese palabras clave...',
                        formatLoadMore     : 'Cargando resultados...',
                        ajax               : {
                            url: data_view,
                            dataType: 'json',
                            data: function (term, page){
                                return {
                                    term: term
                                };
                            },
                            results: function (data, page){

                                if(data !== null){
                                    if(data_new !== undefined){
                                        data[data.length] = {
                                            id: 'create',
                                            text: 'Crear nuevo'
                                        };
                                    }

                                    return { results: data };
                                }
                            }
                        },
                        initSelection: function(element, callback){
                            var defaultTerm	= this_element.attr('value-text');

                            if (defaultTerm !== undefined){

                                $.ajax({
                                    type: "GET",
                                    url: data_view,
                                    data: {
                                        ajaxRequest: true,
                                        term       : defaultTerm
                                    },
                                    dataType: "json",
                                    success: function(data){
                                        //Change if is multiple select
                                        if(data[0]['id'] != 'create'){
                                            callback(data[0]);
                                        }
                                    }, error: function(data){
                                    }
                                });
                            }
                        }
                    };

                    var choosen = this_element.select2(config).data('select2');

                    if(elementSource !== undefined){
                        // Re-initialize with new values
                        elementSource.on('change', function(){
                            this_element.select2('destroy');

                            // If wasn't specified "not refreshing the data-new property"
                            if(dataNewNoSource != 'false'){
                                dataNew	= this_element.attr('data-new')+elementSource.val();
                            }

                            //If wasn't specified "not refreshing the data-view property"
                            if(dataViewNoSource != 'false'){
                                dataView		= this_element.attr('data-view')+elementSource.val();
                                config.ajax.url	= dataView;
                            }

                            // Reset default values
                            this_element.removeAttr('value-text');
                            this_element.attr('value', '');

                            choosen = this_element.select2(config).data('select2');

                            createAddNewButtton(this_element, dataNew);
                        });

                        initTipsyDependenceAlert(this_element, elementSource);
                    }

                    createAddNewButtton(this_element);

                }
            }
        });
    }

    function initTipsyDependenceAlert(this_element, elementSource){
        this_element.on('select2-opening', function(event){
            if(elementSource.val() == '' || elementSource.val() === undefined){
                var elementSourceContainer = elementSource.prev('.select2-container');

                elementSourceContainer.attr('original-title', 'Debe seleccionar una opción.');
                elementSourceContainer.tipsy({
                    gravity: 's',
                    delayIn: 250,
                    offset: 7,
                    fade: true,
                    opacity: .8,
                    trigger: 'manual'
                });

                elementSourceContainer.tipsy('show');

                event.preventDefault();

                setTimeout(function(){ elementSourceContainer.tipsy('hide');}, 2700);
            }
        });
    }


    function createAddNewButtton(this_element, dataNew){
        this_element.on('select2-loaded', function(e){
            try {
                var selectId 				= this_element.attr('select-id');
                var thisElementDrop			= $('.'+selectId);
                var createNewButton			= thisElementDrop.find('.choosen_create');

                if(createNewButton.length){
                    var dropSearchBox			= thisElementDrop.find('.select2-search');
                    var appendedCreateNewButton	= dropSearchBox.find('.choosen_create');
                    var buttonListParent		= createNewButton.parents('.select2-result');

                    if(dataNew === undefined){
                        dataNew = this_element.attr('data-new');
                    }

                    if(appendedCreateNewButton.length){
                        if(this_element.attr('data-new') !== undefined){
                            createNewButton.attr('data-view', dataNew);
                        }

                        buttonListParent.remove();

                    }else{
                        createNewButton.detach()
                            .addClass('add_button')
                            .addClass('mws-tooltip-s')
                            .attr('original-title', 'Agregar nuevo');

                        if(this_element.attr('data-new') !== undefined){
                            createNewButton.attr('data-view', dataNew);
                        }

                        dropSearchBox.append(createNewButton);

                        buttonListParent.remove();

                        createNewButton.on('click', function(e){
                            e.preventDefault();
                            openPanel($(this));
                        });
                    }
                }
            }catch(error){
            }

        });
    }

    body.on('select2-selecting', '[unique-choosen]', function(event){
        event.preventDefault();

        var this_element       = $(this);
        var changedValue       = event.object;

        var unique_group       = this_element.attr('unique-choosen');
        var unique_elements    = $('[unique-choosen="'+unique_group+'"]');
        var withoutCoincidence = true;
        var lastElement        = false;

        unique_elements.each(function(index, el){
            var curren_element = $(this);

            if(curren_element.select2('val') == changedValue.id && this !== this_element[0]){
                withoutCoincidence = false;
            }

            if(index == unique_elements.length-1){
                lastElement = true;
            }
        });

        if(lastElement){
            if(withoutCoincidence){
                this_element.select2('data', changedValue).select2('close').trigger('change');
            }else{
                thisElementContainer = this_element.prev('.select2-container');

                thisElementContainer.attr('original-title', 'Ya se ha seleccionado esa opción.');
                thisElementContainer.tipsy({
                    gravity: 's',
                    delayIn: 250,
                    offset: 7,
                    fade: true,
                    opacity: .8,
                    trigger: 'manual'
                });

                thisElementContainer.tipsy('show');

                event.preventDefault();

                setTimeout(function(){ thisElementContainer.tipsy('hide');}, 2300);
            }
        }
    });

    /* *****							*****************************************************				*/
    /* #####	NUMERIC SPINNERS		#####################################################				*/
    /* *****				 			*****************************************************				*/

    function createNumericSpinners(){
        var numericSpinners = $('.numeric_spinner');

        if(numericSpinners.length){
            numericSpinners.each(function(index, el) {
                var this_element = $(this);

                if(!this_element.hasClass('ui-spinner-input')){

                    var maxValue	= this_element.attr('data-max');
                    var minValue	= this_element.attr('data-min');
                    var step		= this_element.attr('data-step');
                    var format		= this_element.attr('data-format');
                    var disabled	= this_element.attr('data-disabled');

                    var numericSpinnerConfig	= {
                        culture: "en-US"
                    };

                    if(maxValue !== undefined){
                        numericSpinnerConfig.max = maxValue;
                    }

                    if(minValue	!== undefined){
                        numericSpinnerConfig.min = minValue;
                    }

                    if(step !== undefined){
                        numericSpinnerConfig.step = step;

                        numericSpinnerConfig.change = function( event, ui ){
                            var thisSpinner		= $(this);
                            var spinnerValue	= this_element.spinner('value');

                            if($.isNumeric(spinnerValue)){
                                if((spinnerValue % step) !== 0){
                                    //this_element.spinner( "stepUp", 1 );
                                }
                            }else{
                                this_element.val(1);
                                this_element.spinner( "value", 1);
                            }
                        }
                    }

                    if(format !== undefined){
                        numericSpinnerConfig.numberFormat = format;
                    }

                    this_element.spinner(numericSpinnerConfig);

                    if(disabled	== 'true'){
                        this_element.siblings('.ui-spinner-button').css('display', 'none');

                        this_element.on('spin', function(e){
                            e.preventDefault();
                        });
                    }

                    this_element.on('change', function(){

                        var spinnerValue	= this_element.val();

                        spinnerValue = spinnerValue.replace('$', '');
                        spinnerValue = spinnerValue.replace(',', '');

                        if($.isNumeric(spinnerValue)){
                            if(step !== undefined){
                                if((spinnerValue % step) !== 0){
                                    this_element.spinner( "stepUp", 0 );
                                }else{
                                    this_element.spinner('value', parseInt(this_element.val()) );
                                }
                            }
                        }else{
                            this_element.val(1);
                            this_element.spinner( "value",1);
                        }
                    });

                    this_element.on('focus', function(){
                        var thisSpinner		= $(this);
                        var spinnerValue	= this_element.spinner('value');

                        this_element.val(spinnerValue);
                    });

                    this_element.on('blur', function(){
                        var thisSpinner	= $(this);
                        var inputValue	= this_element.val();

                        inputValue = inputValue.replace('$', '');
                        inputValue = inputValue.replace(',', '');

                        if($.isNumeric(inputValue)){
                            this_element.spinner( "value", inputValue);
                        }else{
                            this_element.val(1);
                            this_element.spinner( "value", 1);
                        }
                    });
                }
            });
        }
    }

    /* *****							*****************************************************				*/
    /* #####	TOOLTIP ALERT 			#####################################################				*/
    /* *****				 			*****************************************************				*/

    function addTooltipAlert(this_element, messageText){
        var tooltipTarget;

        if(this_element.hasClass('ajax_choosen')){
            tooltipTarget = this_element.prev('.select2-container');
        }else if(this_element.hasClass('numeric_spinner')){
            tooltipTarget = this_element.parent('.ui-spinner');
        }else{
            tooltipTarget = this_element;
        }

        tooltipTarget.attr('original-title', messageText);
        tooltipTarget.tipsy({
            gravity: 's',
            delayIn: 250,
            offset: 7,
            fade: true,
            opacity: .8,
            trigger: 'manual'
        });

        tooltipTarget.tipsy('show');

        setTimeout(function(){ tooltipTarget.tipsy('hide');}, 2000);
    }

    body.on('click', '.confirm-action', function(e){
        if(!confirm('¿Está seguro de realizar esta acción?')) {
            e.preventDefault();
        }
    });

    function initSwapComponents(){
        createInputDate();
        createNumericSpinners();
        createAjaxChoosen();
    }

    initSwapComponents();

    body.on('click', '.ajax-delete', function(event){
        event.preventDefault();

        var thisLink    = $(this);
        var thisLinkUrl = thisLink.attr('href');

        //Agregar confirm
        $.ajax({
            url    : thisLinkUrl,
            data   : {},
            type   : 'POST',
            success: function(data){
                closePanel();

                var gridView = $('.grid-view');

                if(gridView.length){
                    gridView.yiiGridView('update');
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    });
});

/** Funciones nuevas **/

$(document).ready(function(){
    $(".friends-head").on("click", function(e){
        e.stopPropagation();
        e.preventDefault();

        var $dataActions = $.parseJSON($(this).attr("data-actions"));

        if($(".friend-requests").css("display") == "none"){

            $(".box-messages, .drop-list").fadeOut("fast");
            $(".container-messages .navbar-buttons").removeClass("actual");
            $(this).addClass("actual");

            $(".request-list").load($dataActions.load);

            $.ajax({
                url: $dataActions.ajaxURL,
                dataType: "html",
                success : function(data) {
                    $("#numFriendRequest").html("");
                }
            });

        } else {
            $(this).removeClass("actual");
        }

        $(".friend-requests").fadeToggle("fast");

    });

    var start_limit;
    var end_limit;

    $(".messages-head").on("click", function(e){
        e.stopPropagation();
        e.preventDefault();

        var $dataActions = $.parseJSON($(this).attr("data-actions"));
        start_limit=0;
        end_limit=3;

        if($(".message-inbox").css("display") == "none"){

            $(".box-messages, .drop-list").fadeOut("fast");
            $(".container-messages .navbar-buttons").removeClass("actual");
            $(this).addClass("actual");

            $(".message-list").load($dataActions.load, { "start_limit": start_limit,"end_limit": end_limit });

            $.ajax({
                url: $dataActions.ajaxURL,
                dataType: "html",
                success : function(data) {
                    $("#numMessage").html("");
                }
            });
        } else {
            $(this).removeClass("actual");
        }

        $(".message-inbox").fadeToggle("fast");
    });

    $(".message-list").on("scroll", function(){
        if ($(".message-list").scrollTop() == $("#preview-messages").height()-$(".message-list").height()){

            var $dataActions = $.parseJSON($(this).attr("data-actions"));
            start_limit += end_limit;
            end_limit += 3;

            $("<div>").load(
                $dataActions.load,
                { "start_limit" : start_limit, "end_limit" : end_limit },
                function(response, status, xhr) {
                    if(response.indexOf("empty")  == -1){
                        $(".message-list").append(response);
                    }
                }
            );
        }
    });

    var notificationClicked = false;
    var notificationPage = 1;
    var notificationPageCount = 100;

    $(".notification-head").click(function(e){
        e.stopPropagation();
        e.preventDefault();

        var $dataActions = $.parseJSON($(this).attr("data-actions"));
        if($(".notification-requests").css("display") == "none"){

            $(".box-messages, .drop-list").fadeOut("fast");
            $(".container-messages .navbar-buttons").removeClass("actual");
            $(this).addClass("actual");

            $.ajax({
                url: $dataActions.load,
                dataType: "html",
                success : function(data) {
                    $(".notification-requests .request-list").html(data);
                    // Reset the current page
                    notificationPage = 1;
                },
                beforeSend: function (returnedData) {
                    if (!notificationClicked) {
                        notificationClicked = true;
                        $(".notification-requests .request-list").html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw" style="position:absolute; margin:auto; top:0; left:0; right:0; bottom:0;"></i>');
                    }
                }
            });

            $.ajax({
                url: $dataActions.ajaxURL,
                dataType: "json",
                success : function(data) {
                    if (data.success) {
                        $("#numNotification").html("");
                    }
                }
            });

        } else {
            $(this).removeClass("actual");
        }

        $(".notification-requests").fadeToggle("fast");
    });

    $(".request-list").on("scroll", function(){
        if ($(".request-list").scrollTop() == $("#preview-notification-requests").height()-$(".request-list").height()){
            var $dataActions = $.parseJSON($(".notification-head").attr("data-actions"));

            if (notificationPageCount > notificationPage) {
                // Set the next page
                notificationPage++;
                $.ajax({
                    url: $dataActions.load,
                    data: { "page" : notificationPage},
                    dataType: "html",
                    success : function(data, status, xhr) {
                        $("#loading-more").addClass('hidden');
                        notificationPageCount = Number(xhr.getResponseHeader('Notifier-Page-Count'));
                        if(data.indexOf("empty")  == -1){
                            $("#preview-notification-requests").append($(data).filter("#preview-notification-requests").html());
                        }
                    },
                    beforeSend: function (returnedData) {
                        $("#loading-more").removeClass('hidden');
                    }
                });
            }
        }
    });

    $(document).on("click", function (e) {
        $(".box-messages").fadeOut("fast");
        $(".container-messages .navbar-buttons").removeClass("actual");
    });
});

/* categoryMarket */
/**
 * Funcion que verifica que no se repitan los subrubros.
 * @param string current El id selector del campo posicionado actualmente.
 */
function verifySame(current, mensajes) {
    var count = 0;
    $("input:text[id*='SubcategoryMarketModel_']").each(function(index, element){
        if($(element).val() !== ""){
            if($(element).val() == $(current).val()){
                count++;
            }

            if (count > 1 ){
                alert(mensajes.catExist + " " + $(current).val());
                $(current).val("");
                e.preventDefault();
            }
        }
    });
}

$(document).ready(function(){
    $(".container_dinamic_subcategory").on("change", "input[id*='SubcategoryMarketModel_']", function(e){
        var current = "#" + $(this).attr("id");
        verifySame(current, mensajes);
    });

    /**
     * Crea los campos dinamicamente.
     **/
    var numElement = $(".container_dinamic_subcategory").find(".subcategory").length + 1;

    $(".agrega_campo a").click(function(e){

        var fields;
        numElement = numElement + 1;

        e.preventDefault();

        fields = '<div class="form-group dinamic_field subcategory" >';
        fields += '<div class="col-sm-5">';

        fields += '<input class="input-sm" placeholder="Escriba la subcategoría ..." name="SubcategoryMarketModel['+ numElement +'][title]" id="SubcategoryMarketModel_'+ numElement +'_title" type="text" maxlength="75">';
        fields += '<input name="SubcategoryMarketModel['+ numElement +'][id]" id="SubcategoryMarketModel_'+ numElement +'_id" type="hidden">';

        fields += '</div>';
        fields += '<div class="col-sm-5">';

        fields += '<input class="input-sm" placeholder="Escriba la traducción ..." name="SubcategoryMarketModel['+ numElement +'][translation]" id="SubcategoryMarketModel_'+ numElement +'_translation" type="text" maxlength="75">';
        fields += '<input name="SubcategoryMarketModel['+ numElement +'][message_id]" id="SubcategoryMarketModel_'+ numElement +'_message_id" type="hidden">';

        fields += '</div>';
        fields += '<a class="delete_field" href="#"><span class="btn alert-success"><span class="fa fa-minus"></span></span></a>';
        fields += '</div>';

        $(".container_dinamic_subcategory").append(fields);

    });

    /**
     * Elimina los campos dinamicamente.
     **/
    $("body").on("click", ".delete_field", function(e){
        $(this).parent("div").remove();
        return false;
    });
});

/* friends */
function validateText(e){
    //acepta barra espaciadora, retroceso, supr y flechas
    var key = e.which || e.keyCode;
    if((key != 32) && (key != 08) && (key != 46) && (key != 37) && (key != 38) && (key != 39) && (key != 40) && (key < 65) || (key > 90) && (key < 97) || (key > 122)){
        return true;
    }
    return false;
}

function validateFriendText(e){
    //acepta barra espaciadora, retroceso,enter, supr y flechas
    var key = e.which || e.keyCode;
    if((key != 32) && (key != 08) && (key != 13) && (key != 46) && (key != 37) && (key != 38) && (key != 39) && (key != 40) && (key < 65) || (key > 90) && (key < 97) || (key > 122)){
        return true;
    }
    else{
        if(((key == 32) || (key == 08)) && $('input#friend').val().trim()==''){
            return true;
        }
    }
    return false;
}

$(document).ready(function(){
    var ajaxUpdateTimeout;
    var ajaxRequest;
    $('input#term').keyup(
        function(event){ //al presionar una tecla realizar búsqueda
            if(validateText(event)){
                event.preventDefault();
            }else{//debe contener letras o texto
                if($('input#term').val().trim()!='' || $('input#term').val()==''){
                    ajaxRequest = $(this).serialize();
                    clearTimeout(ajaxUpdateTimeout);
                    ajaxUpdateTimeout = setTimeout(function () {
                            $.fn.yiiListView.update(
                                // this is the id of the CListView
                                'friend-requests',
                                {data: ajaxRequest}
                            )
                        },
                        // tiempo espera
                        300);
                }
            }
        })
        .keydown(function(event) {//evitar que letras no válidas se muestren en el input#term
            if(validateText(event)){
                event.preventDefault();
            }
        });

    $('input#friend').keydown(function(event){
        if(validateFriendText(event)){
            event.preventDefault();
        }
    });

    /**
     *
     * Busquedas en los gridview.
     */

    $('#search-my-assignment').change(function(e) {

        e.preventDefault();
        $.fn.yiiGridView.update('focus-group-user-grid', {

            data: $(this).serialize()

        });

        return false;

    });


    $('#search-aviliable').change(function(e) {

        e.preventDefault();
        $.fn.yiiGridView.update('focus-group-grid', {

            data: $(this).serialize()

        });

        return false;

    });

    /**
     * Botón cargar mas.
     */

    $('#load_more').click(function(e){
        /**
         * TODO: Aplicar la busqueda por rangos ya que es la manera mas optima.
         */
        e.preventDefault();
        var numRows = $('#focus-group-grid div >table >tbody >tr').length;
        numRows = numRows + 5;

        $.fn.yiiGridView.update('focus-group-grid', {

            data:  {limitIncrement : numRows }

        });

    });

    /**
     * Cambio de botones en perfil.
     */

    $(document).on('click', '.btn-switch', function(e) {
        e.preventDefault();

        $.ajax({
            type: "GET",
            dataType: "json",
            url: $(this).attr("href"),
            success: function(data) {

                $('#container_buttons').html("");

                $.each(data, function(index, value) {

                    $('#container_buttons').append(value);

                });

            }
        });


    });

    /**
     * Funcion checkbox premios
     **/
    $(document).on('click', '.checkboxPremio', function() {
        if(this.checked){
            $('#thumb_'+$(this).attr('value')).addClass("selected");
        }else{
            $('#thumb_'+$(this).attr('value')).removeClass("selected");
        }

        var ajaxURL = $(this).attr("data-ajaxURL");
        var dataName = $(this).attr('name');
        var dataSerialize = $('input[name="' + dataName + '"]').serialize();

        $.ajax({
            type: 'POST',
            url: ajaxURL,
            data: dataSerialize,
            dataType: 'json',
            success: function(data){
                $('#alert_swapping').html(data.message);
                if(data.disabled){
                    $('#btn_swapping').attr('disabled', 'disabled');
                    $('#alert_swapping').addClass('alert alert-danger');
                }else{
                    $('#btn_swapping').removeAttr('disabled');
                    $('#alert_swapping').removeClass('alert alert-danger');
                    $('#alert_swapping').addClass('alert alert-success');
                }
            }
        });
    });

    /**
     * Funcion boton aceptar solicutud de amistad
     **/

    $(document).on('click', '.accept-friend-request', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var ajaxURL = ($(this).attr("href"));

        $.ajax({
            type: 'GET',
            url: ajaxURL + "&metodo=ajax",
            dataType: 'json',
            success: function(data){
                if(data.status == "true"){
                    $.fn.yiiGridView.update('friend-grid');
                }
            }
        });
    });


    $('.tabs-emojis').on('click', '.nav-tabs a', function(){
        // set a special class on the '.dropdown' element
        $(this).closest('.dropdown').addClass('dontClose');
    })

    $('.dropdown-emojis').on('hide.bs.dropdown', function(e) {
        if ( $(this).hasClass('dontClose') ){
            e.preventDefault();
        }
        $(this).removeClass('dontClose');
    });


});


/*
 * Evita doble click en todos formularios menos en los formularios
 * donde se realiza una llamada ajax.
 */
var activeRequestBlockForm = false;

$(document).on('submit',"form:not(.submitAjax)",function(e){

    var form = $(this);

    if(!activeRequestBlockForm){

        blockSubmit(form);
        activeRequestBlockForm = true;

    }else{

        e.preventDefault();

    }

});

/*
 * Funcion que bloquea el submit del formulario, cuando existen validaciones por 
 * ajax.
 */
function blockSubmit(form){

    form.find("input[type=submit]").attr("disabled", "disabled");
    form.find("input").attr("readonly", true);
    document.body.style.cursor = "wait";

}


/**
 *  Funcion que desbloquea el submit del formulario, cuando existen validaciones
 *   por ajax.
 */
function submitDefault(form){

    form.find("input[type=submit]").attr("disabled", false);
    form.find("input").attr("readonly", false);

}
