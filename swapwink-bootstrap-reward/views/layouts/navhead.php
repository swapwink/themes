<?php

use common\models\Category;
use common\models\Reward;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\WinksUserActivity;

$location = (isset($this->params['location']))? $this->params['location'] : null;
$languageInfo = User::getInfoToggleLanguage();
$urlChangeLang = Url::toRoute(['/site/changeLanguage', 'lang' => $languageInfo['language']]);

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest):

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'My winks'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
        'icon' => 'fa fa-star-o'
    ];

    $myWishList = [
        'label' => Yii::t('commonTheme', 'Wish list'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/myWishList'),
        'icon' => 'fa fa-heart-o',
    ];

    $history = [
        'label' => Yii::t('commonTheme', 'My rewards'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias . '?section=history'),
        'icon' => 'fa fa-truck',
    ];

    switch (Yii::$app->user->identity->user_type):
        case User::PARTICIPANT:
        case User::AFFILIATE:
            $controlPanelMenu[] = $myWishList;
            $controlPanelMenu[] = $history;
            break;
    endswitch;

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'ico-custom ico-logout'
    ];

    $numNotifications = Yii::$app->notifier->count();
endif;

$currentUrlForSearchForm = Url::current();
?>

<?php
$this->registerCss('
    .download-app-header .link{
        color: #fff;
        border-bottom: 1px solid #fff;
        margin: 0 30px;
    }
    .download-app-header .link:hover{
        color: #fff;
    }
    
    a .fa.fa-heart-o, a .fa.fa-truck, a .fa.fa-star-o{
        color:#d6d6d6;
        font-size:24px;
        margin: 0 10px;
    }
    
    a .fa.fa-heart-o:hover, .drop-list ul li a:hover .fa{
        color:#d6d6d6;
    }
    
    .navbar-head .control-panel .drop-list span.arrow{
        border-bottom: 10px solid #ffffff;
    }
');?>

<?php
$filters = [];
$allRewardsAvailable = new ActiveDataProvider([
    'query' => Reward::getCommonQuery($filters)
]);

$categoryProvider = Category::getCategoriesListTranslated();
$categorySelected = '';

if (isset(Yii::$app->user->identity) && empty(Yii::$app->session->get('winks'))) {
    $user = User::findOne(Yii::$app->user->id);
    $winks = WinksUserActivity::find()->where(['swap_id' => $user->swap_id])->sum('winks');
    Yii::$app->session->set('winks', $winks);
}
?>


<div class="navbar-wrapper navbar">
    <div class="download-app-header">
        <div class="container">
            <div class="pull-right">
                <section>
                    <?php if(isset(Yii::$app->params['linkBlog'])):?>
                        <a target="_blank" href="<?php echo Yii::$app->params['linkBlog']?>" class="link">Blog</a>
                    <?php endif;?>

                    <?=Yii::t('commonTheme', 'Download our app')?>
                    <a target="_blank" href="<?= Yii::$app->params['appleAppLink']?>">
                        <i class="fa fa-apple"></i>
                    </a>
                    <a target="_blank" href="<?= Yii::$app->params['androidAppLink']?>">
                        <i class="fa fa-android"></i>
                    </a>
                    <div class="dropdown">
                        <a id="dLabel" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="<?=User::getInfoLanguage()['icon']?>"></div>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>
                                <a href="<?=$urlChangeLang?>">
                                    <div class="<?=$languageInfo['icon']?>"></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="navbar-head">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <a id="home_url" href="<?= Yii::$app->homeUrl ?>">
                        <?= Html::img("https://d224v694zcq7e8.cloudfront.net/themes/coupon/img/logotype.png", ['class' => 'navbar-brand', 'alt' => Yii::$app->name]); ?>
                    </a>
                </div>

                <div class="col-md-2">
                    <?= $this->render('//site/_categories_select_location', [
                        'categoryProvider' => $categoryProvider,
                        'categorySelected' => $categorySelected,
                        'allRewardsAvailable' => $allRewardsAvailable,
                        'filters' => $filters
                    ]); ?>
                </div>

                <div class="col-md-4 hidden-xs hidden-sm">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'class' => 'search-form',
                        'method' => 'get',
                        'action' => $currentUrlForSearchForm
                    ]);
                    ?>

                    <input type="hidden" name="slug" value="<?= (Yii::$app->request->get('slug')) ? Yii::$app->request->get('slug') : "" ?>">

                    <div class="coupon-search-content<?= (!empty($this->params['hideSearchReward']) && $this->params['hideSearchReward']) ? " hidden" : "" ?>">
                        <?= Html::textInput('search_string', '', ['placeholder' => Yii::t('commonTheme', 'Search'), 'maxlength' => 40]); ?> <i class="fa fa-search"></i>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="row">
                        <?php if (!\Yii::$app->user->isGuest): ?>
                            <div class="control-panel pull-right">
                                <div class="user-info button_switch">
                                    <div class="user-avatar pull-left">
                                        <figure>
                                            <?php echo Html::img(Yii::$app->user->identity->urlAvatar, ['id' => 'avatar_profile', 'class' => 'user-avatar']); ?>
                                        </figure>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div><!--.user-info-->

                                <div class="drop-list">
                                    <span class="arrow"></span>
                                    <ul>
                                        <?php
                                        foreach ($controlPanelMenu as $item) :
                                            if (!empty($item['url'])) :
                                                $linkLabel = '<span class="' . $item['icon'] . '"></span>' . $item['label'];
                                                echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                                            else:
                                                echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                                            endif;
                                        endforeach;
                                        ?>
                                    </ul>
                                </div><!--.drop-list-->
                            </div><!--.control-panel-->

                            <div class="container-messages-notifications">
                                <div class="notification-header container-messages">
                                    <a class="notification-head navbar-buttons" data-actions='{"load":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/preview") ?>", "ajaxURL":"<?= Yii::$app->urlManager->createAbsoluteUrl("notifier/check-as-read") ?>"}'>
                                        <span class="fa fa-bell"></span>
                                        <span id="numNotification" class="label"><?= ($numNotifications > 0) ? $numNotifications : '' ?></span>
                                    </a>
                                    <div class="box-messages notification-requests">
                                        <span class="arrow"></span>
                                        <div class="request-list"></div>
                                        <div class='view-all'><?= Html::a(Yii::t('commonTheme', 'view all'), Yii::$app->urlManager->createUrl('notifier/index')) ?></div>
                                    </div><!--.notification-requests-->
                                </div><!--.notification-header-->
                            </div><!--.container-messages-notifications-->
                            <?php
                        else :
                            if (!empty(Yii::$app->session->get('preregister'))) :
                                ?>
                                <div class="control-guest pull-right">
                                    <a href="#" class="a-guest" style="text-align: right">
                                        <?= Yii::$app->session->get('preregister')->first_name ?>
                                        <?= Yii::$app->session->get('preregister')->last_name ?>
                                        <br>
                                        <?= Yii::$app->session->get('preregister')->email ?>
                                    </a>
                                </div>
                            <?php else :
                                ?>
                                <div class="control-guest pull-right">
                                    <a href="<?= Yii::$app->homeUrl ?>" class="a-guest">
                                        <?= Yii::t('commonTheme', 'Login / Register'); ?>
                                    </a>
                                </div><!--.control-panel-->
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!--.navbar-head-->
</div><!--.navbar-wrapper-->
