<?php
use common\widgets\Alert;
use swapwink\themes\AppAssetReward;
use yii\helpers\Html;
use yii\helpers\Url;
AppAssetReward::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage ?>" lang="<?= Yii::$app->language ?>">
	<?= $this->render('//layouts/header') ?>
	<body>
    <?php $this->beginBody() ?>
        <div id="wrap">
            <?= $this->render('//layouts/navhead') ?>
            <?= Alert::widget() ?>
            <?= $content; ?>
        </div>
		<?= $this->render('//layouts/footer') ?>
		<?php $this->endBody() ?>
	</body>
</html>

<?php $this->endPage() ?>
