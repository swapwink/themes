<?php
use yii\helpers\Html;

$menu[] = [
    'label' => Yii::t('commonTheme', 'Surveys'), 
    'url' => Yii::$app->urlManager->createAbsoluteUrl('survey'),
    'icon' => 'fa fa-check-square-o'
];

$menu[] = [
    'label' => Yii::t('commonTheme', 'Focus Group'), 
    'url' => Yii::$app->urlManager->createAbsoluteUrl('focus_group'),
    'icon' => 'fa fa-comments-o'
];
?>

<div class="menu-bar">

  <!--   <h2><?php echo Yii::t('commonTheme', 'Welcome, where do you want to start?'); ?></h2> -->

	<ul>
        <?php foreach ($menu as $item):?>
        <?php $linkLabel = '<span class="fa '.$item['icon'].'"></span>'.$item['label'] ?>
        <?= Html::tag('li', Html::a($linkLabel, $item['url']), [])?>
        <?php endforeach;?>
	</ul>

</div> <!-- End menu-bar -->