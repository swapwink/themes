<?php

namespace swapwink\themes;

use yii\web\AssetBundle;

class AppAssetAdmin extends AssetBundle
{

    public $sourcePath;
    public $basePath;
    public $baseUrl;
    public $css = [
        'css/main.less'
    ];
    public $js = [
        'js/plugins.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $cssOptions = ['position' => \yii\web\View::POS_END];

    public function init()
    {
        $this->sourcePath = !YII_DEBUG ? '@vendor/swapwink/themes/swapwink-admin' : null; //si esta definida realiza assetManager->publish()
        $this->basePath = !YII_DEBUG ? null : '@vendor/swapwink/themes/swapwink-admin';
        $this->baseUrl = !YII_DEBUG ? null : '/vendor/swapwink/themes/swapwink-admin/';

        parent::init();

        // restablecer BootstrapAsset para que no cargue sus archivos js que causan conflicto
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'js' => []
        ];
    }
}
