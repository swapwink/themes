<?php

namespace swapwink\themes;

use Yii;
use yii\helpers\Html;

class MenuHelper{
	public static function printMenu($items){
		foreach ($items as $item) :
			if (!isset($item['separator'])) :
				if (!empty($item['url'])) :
					if (isset($item['icon']['type'])) :
						$icon = "<i class='" . $item['icon']['class'] . "'></i>";
					else :
						$icon = Html::img($item['icon']);
					endif;

					if (isset($item['label'])) {
						$icon.= '<span class="legend">'.$item['label'].'</span>';
					}

					$class = isset($item['class']) ? $item['class'] : '';
					$classLink = '';

					if(!empty($item['requestedRoute']) && in_array(Yii::$app->requestedRoute, $item['requestedRoute'])){
						$classLink = ' current ';
					}

					if(!empty($item['controller']) && Yii::$app->controller->id == $item['controller']){
						$classLink = ' current ';
					}

					echo Html::tag('li', Html::a($icon, $item['url'], ['class' => $classLink]), ['title' => $item['label'], 'class' => 'carousel-cell' . ' ' . $class]);
				elseif(!isset($item['type'])) :
					echo Html::tag('li', $item['label'], ['class' => 'text-item carousel-cell']);
				endif;
			else :
				echo Html::tag('li', '', ['class' => 'separator']);
			endif;

			if(!empty($item['type']) && $item['type'] == 'submenu') {
				echo '
				<li class="header-submenu-content">
					<a id="' . $item['id'] . '" class="header-menu" data-toggle="' . $item['data-toggle'] . '" href="#">' . $item['label'] . ' <span class="fa fa-chevron-down"></span></a>
				</li>';

				self::printMenu($item['items']);

				echo '
					</ul>
				</li>';
			}
		endforeach;
	}
}