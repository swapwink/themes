Yii2 Swapwink Themes
===================
Tema para proyectos Swapwink en Yii2

Requirements
------------
- Yii2

Installation
------------

La manera recomendada es por medio de [composer](http://getcomposer.org/download/).

Ya sea 

```
composer require swapwink/themes
```

O en la sección require de tu `composer.json`

```json
"swapwink/themes": "*"
```

Una vez instalado, debes agregar el siguiente código a la lista de components de tu proyecto Yii2

'components' => [
	...
	
	'view' => [
        'theme' => [
            'pathMap' => [ 
	            '@frontend/views' => [ 
	                '@vendor/swapwink/themes/swapwink-bootstrap/views'
	            ]
            ],
        ],
    ],
	
	...
],

Usage
-----
```
use swapwink\themes\AppAsset;

AppAsset::register($this);
```

