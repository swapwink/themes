<?php
use swapwink\themes\AppAssetCoreAdmin;
use common\widgets\Alert;

AppAssetCoreAdmin::register($this);

$this->beginPage();

if(isset($this->params['pageTitle'])){
    $this->title = $this->params['pageTitle'];
}

$titlePage = $this->title;

?>
<!DOCTYPE html>
<?= $this->render('//layouts/header', ['titlePage' => $titlePage])?>
<body class="<?= !empty(Yii::$app->view->params['bodyClass']) ? Yii::$app->view->params['bodyClass'] : '' ?>">
    <?php $this->beginBody() ?>

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="outer-wrapper">
        <?= $this->render('//layouts/nav-lateral') ?>
        <div class="wrapper">
            <?= $this->render('//layouts/navhead', ['titlePage' => $titlePage]) ?>

            <div role="main" id="main-wrap">
                <?= Alert::widget() ?>
                <?= $content ?>
                <div class="clearfix"></div>
            </div>
            <?= $this->render('//layouts/footer') ?>
        </div><!--.wrapper-->
    </div><!--.outer-wrapper-->

    <?php
    if (!Yii::$app->request->isAjax && Yii::$app->id != 'swapwink/mybusinessBackend') { ?>
        <!-- Start of swapwink Zendesk Widget script -->
        <script>/*<![CDATA[*/window.zE||(function(e,t,s){var n=window.zE=window.zEmbed=function(){n._.push(arguments)}, a=n.s=e.createElement(t),r=e.getElementsByTagName(t)[0];n.set=function(e){ n.set._.push(e)},n._=[],n.set._=[],a.async=true,a.setAttribute("charset","utf-8"), a.src="https://static.zdassets.com/ekr/asset_composer.js?key="+s, n.t=+new Date,a.type="text/javascript",r.parentNode.insertBefore(a,r)})(document,"script","baaf8153-2f95-471a-9603-f5739f30c5ff");/*]]>*/

        <?php
        if(Yii::$app->user->identity){
            $user_name = Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name;
            $user_email = Yii::$app->user->identity->email;

            $session = Yii::$app->session;
            if(isset($session['affiliate'])){
                $user_name .= " [". $session['affiliate']->affiliate_name ."]";
            }
            ?>

            zE( function () { 
            var userName = "<?=$user_name?>"; 
            var userEmail = "<?=$user_email?>"; 
            zE.identify({name: userName, email: userEmail}); 
            });

        <?php
        }?>

        </script>
    <?php
    } ?>

    <?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
