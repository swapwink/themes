<?php
use yii\helpers\Html;
use common\models\User;

$sideMenu = [];

if (!\Yii::$app->user->isGuest) :
    $solutions =[
        'coupon' => [
            $coupons = [
                'label' => Yii::t('commonTheme', 'Products'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
                'requestedRoute' => ['coupon/index', 'coupon/create', 'coupon/view', 'coupon/update']
            ],
            $couponExchange = [
                'label' => Yii::t('commonTheme', 'Redeem'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/exchange'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-barcode.png",
                'requestedRoute' => ['coupon/exchange']
            ],
            $sold = [
                'label' => Yii::t('commonTheme', 'Sold Products'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/sold'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
                'requestedRoute' => ['coupon/sold']
            ],
            $earnings = [
                'label' => Yii::t('commonTheme', 'Earnings'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('product/report-sale'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-earnings.png",
                'requestedRoute' => ['product/report-sale']
            ],
            /* Se oculta chat
            $couponChat = [
                'label' => Yii::t('commonTheme', 'Chat'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/chat'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'users'
                ],
                'requestedRoute' => ['coupon/chat']
            ],*/
            /**
             * TODO: Se oculta sección de winkboard hasta que se defina como se implementara en el nuevo flujo
             */
            /*
            $winkboard = [
                'label' => Yii::t('commonTheme', 'Winkboard'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('billboard/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-winkboard.png",
                'controller' => 'billboard'
            ],
            */
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon-sticker/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-sticker.png",
                'controller' => 'coupon-sticker'
            ],
            $couponReports = [
                'label' => Yii::t('commonTheme', 'Reports'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/report'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-reports.png",
                'requestedRoute' => ['coupon/report']
            ],
        ],
        'reward' => [
            $rewards = [
                'label' => Yii::t('commonTheme', 'Rewards'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/index'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'gift'
                ],
                'requestedRoute' => ['reward/index', 'reward/create', 'reward/view', 'reward/update']
            ],
            $rewardExchange = [
                'label' => Yii::t('commonTheme', 'Exchange'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward/exchange'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'barcode'
                ],
                'requestedRoute' => ['reward/exchange']
            ],
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('reward-sticker/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-sticker.png",
                'controller' => 'reward-sticker'
            ],
        ],
        'contest' => [
            $contest = [
                'label' => Yii::t('commonTheme', 'Contest'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('contest/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-contest.png",
                'requestedRoute' => ['contest/index']
            ]
        ],
        'ask' => [
            $ask = [
                'label' => Yii::t('commonTheme', 'Surveys'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('ask/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-surveys.png",
                'requestedRoute' => ['ask/index']
            ],
            $sticker = [
                'label' => Yii::t('commonTheme', 'Sticker'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('ask-sticker/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-sticker.png",
                'controller' => 'ask-sticker'
            ],
        ],
        'b2b-coupon' => [
            $coupons = [
                'label' => Yii::t('commonTheme', 'B2B Coupons'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/index'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-coupons.png",
                'requestedRoute' => ['b2b-coupon/index', 'b2b-coupon/create', 'b2b-coupon/view', 'b2b-coupon/update']
            ],
            $couponExchange = [
                'label' => Yii::t('commonTheme', 'Redeem'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/exchange'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-barcode.png",
                'requestedRoute' => ['b2b-coupon/exchange']
            ],
            $couponChat = [
                'label' => Yii::t('commonTheme', 'Chat'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/chat'),
                'icon' => [
                    'type' => 'font',
                    'class' => 'users'
                ],
                'requestedRoute' => ['coupon/chat']
            ],
            $couponReports = [
                'label' => Yii::t('commonTheme', 'Reports'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('b2b-coupon/report'),
                'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-sw-reports.png",
                'requestedRoute' => ['b2b-coupon/report']
            ],
        ]
    ];

    $groupAffiliate = [
        [
            'type' => 'submenu',
            'label' => 'General',
            'data-toggle' => '.sub-general',
            'id' => 'menuGeneral',
            'items' => [
                $credits = [
                    'label' => Yii::t('commonTheme', 'Credits'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge'),
                    'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-credits.png",
                    'class' => 'soft-hide sub-general',
                    'controller' => 'recharge'
                ],
                $subsidiaries = [
                    'label' => Yii::t('commonTheme', 'Locations'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('subsidiary'),
                    'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-locations.png",
                    'class' => 'soft-hide sub-general',
                    'controller' => 'subsidiary'
                ],
                $devices = [
                    'label' => Yii::t('commonTheme', 'Devices'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('device'),
                    'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-device.png",
                    'class' => 'soft-hide sub-general',
                    'controller' => 'device'
                ],
                $roles = [
                    'label' => Yii::t('commonTheme', 'Administrators'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('role'),
                    'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-role.png",
                    'class' => 'soft-hide sub-general',
                    'controller' => 'role'
                ],
                $employees = [
                    'label' => Yii::t('commonTheme', 'Employees'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('employee-affiliate'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'users'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'employee-affiliate'
                ],
                $thirParty = [
                    'label' => Yii::t('commonTheme', 'Third Party'),
                    'url' => Yii::$app->urlManager->createAbsoluteUrl('third-party'),
                    'icon' => [
                        'type' => 'font',
                        'class' => 'cubes'
                    ],
                    'class' => 'soft-hide sub-general',
                    'controller' => 'third-party'
                ]
            ]
        ]
    ];

    $groupAdmin =[
        $users = [
            'label' => Yii::t('commonTheme', 'Users'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-user.png",
        ],
        $affiliates = [
            'label' => Yii::t('commonTheme', 'Affiliates'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png",
        ],
        $cocolect = [
            'label' => Yii::t('commonTheme', 'Cocolect'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('cocolect'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-lateral-locations.png",
            'requestedRoute' => ['cocolect']
        ],
        $commission = [
            'label' => Yii::t('commonTheme', 'Commission'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('commission'),
             'icon' => [
                'type' => 'font',
                'class' => 'percent'
            ],
            'requestedRoute' => ['commission']
        ],
        $preregister = [
            'label' => Yii::t('commonTheme', 'Pre-registered'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('preregister'),
            'icon' => [
                'type' => 'font',
                'class' => 'user-plus'
            ]
        ],
        $recruiter = [
            'label' => Yii::t('commonTheme', 'Recruiters'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-recruitment.png",
        ],
        $report = [
            'label' => Yii::t('commonTheme', 'Reports'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('site/report'),
            'icon' => [
                'type' => 'font',
                'class' => 'area-chart'
            ],
        ],
        $deletingImage = [
            'label' => Yii::t('commonTheme', 'Images'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('site/deleting-image'),
            'icon' => [
                'type' => 'font',
                'class' => 'file-image-o'
            ],
        ],
        $massMailing = [
            'label' => Yii::t('commonTheme', 'Mass mailing'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('email-editing-template'),
            'icon' => [
                'type' => 'font',
                'class' => 'envelope-o'
            ],
        ],
        $massMailing = [
            'label' => Yii::t('commonTheme', 'Partner'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('partner'),
            'icon' => [
                'type' => 'font',
                'class' => 'code'
            ],
        ],
        $recomendation = [
            'label' => Yii::t('commonTheme', 'Recomendations'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recomendation'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png",
        ],
    ];
    
    $groupSubsidiary = [
        $couponExchange = [
            'label' => Yii::t('commonTheme', 'Redeem'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/exchange'),
            'icon' => [
                'type' => 'font',
                'class' => 'barcode'
            ]
        ],
        $devices = [
            'label' => Yii::t('commonTheme', 'Devices'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('device'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-device.png",
        ]
    ];
    
    $groupRecruiter = [
        $recruiterCommissions = [
            'label' => Yii::t('commonTheme', 'Commissions'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter/commission'),
            'icon' => [
                'type' => 'font',
                'class' => 'money'
            ],
        ],
        $referred = [
            'label' => Yii::t('commonTheme', 'Referred'),
            'url' => Yii::$app->urlManager->createAbsoluteUrl('recruiter/referred'),
            'icon' => Yii::$app->params['cdnPathAdmin'] . "/img/ico-affiliates.png"
        ]
    ];
    
    $groupParticipant = [
        /* 
         * TODO: Evaluar donde es mejor incluir el módulo de partner.
         * 
         * $myApps = [
         *   'label' => Yii::t('commonTheme', 'Partners'),
         *   'url' => Yii::$app->urlManager->createAbsoluteUrl('partner/index'),
         *   'icon' => [
         *       'type' => 'font',
         *       'class' => 'code'
         *  ],
         * ],
         */
        $profile = [
            'label' => Yii::t('commonTheme', 'Profile'),
            'url' =>  Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
            'icon' => [
                'type' => 'font',
                'class' => 'user'
            ]
        ]
    ];


    switch (Yii::$app->user->identity->user_type) :
        case User::ADMIN:
            $sideMenu = array_merge($sideMenu, $groupAdmin);
            
            if (Yii::$app->getModule('moduleManager') != null && count(Yii::$app->getModule('moduleManager')->getInstalledModules()) > 0) {
                foreach (Yii::$app->getModule('moduleManager')->getInstalledModulesByRoleForThemes('admin') as $format) {
                    $sideMenu[] = $format;
                }
            }   
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['backend/dashboard']);
            break;
        case User::AFFILIATE:
            if (!empty(Yii::$app->session->get('sectionName'))) {
                $sideMenu = array_merge($sideMenu, $solutions[Yii::$app->session->get('sectionName')]);
            }
            $sideMenu = array_merge($sideMenu, $groupAffiliate);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['affiliate/dashboard']);
            break;
        case User::SUBSIDIARY:
            $sideMenu = array_merge($sideMenu, $groupSubsidiary);
            break;
        case User::PARTICIPANT:
            $sideMenu = array_merge($sideMenu, $groupParticipant);
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
            break;
        case User::RECRUITER:
            $sideMenu = array_merge($sideMenu, $groupRecruiter);
            break;
    endswitch;

endif;

$this->registerCss('
.menu-lateral .dropdown-menu{
    position: static;
    float: none;
    width: auto;
    min-width: auto;
    background: transparent;
    margin: 0 0 0 0;
    padding: 0 0 0 0;
    text-align: center;
    border: 0 none;
    border-radius: 0;
    box-shadow: none;
    top: initial;
    left: initial;
}

.menu-lateral ul a span.legend{
   position: static;
   padding: 0 0;
   margin: 0 0;
   display: block;
   line-height: initial;
   height: auto;
}

.menu-lateral .dropdown-menu > li > a{
    padding: 0 0;
}

.menu-lateral .dropdown-menu > li > a:hover{
    color: #fff;
}

.menu-lateral .header-menu{
    height: auto!important;
    line-height: initial!important;
}

.soft-hide{
    display: none;
}

.menu-lateral .header-menu{
    border-top: 1px solid #181818;
    border-bottom: 1px solid #181818;
    padding: 8px 0;
    font-size: 12px;
    border-left: transparent!important;
    position: relative;
    overflow: hidden;
}

.menu-lateral .header-menu .fa.fa-chevron-down,
.menu-lateral .header-menu .fa.fa-chevron-up{
    color: #21d397;
}


.menu-lateral .header-menu::after{
    content: "";
    display: block;
    position: absolute;
    height: 50%;
    width: 60%;
    top: -20px;
    left: 20%;
    box-shadow: 0 0 10px 20px rgba(70,70,70,.6);
    border-radius: 100%;
    z-index: -1;
}

@media only screen and (max-width: 600px){
    .menu-lateral .header-submenu-content{
        display: none!important;
    }
    
    .menu-lateral .soft-hide{
        display: block;
    }
}');

function printMenu($items){
    foreach ($items as $item) :
        if (!isset($item['separator'])) :
            if (!empty($item['url'])) :
                if (isset($item['icon']['type'])) :
                    $icon = "<i class='fa fa-" . $item['icon']['class'] . " fa-2'></i>";
                else :
                    $icon = Html::img($item['icon']);
                endif;

                if (isset($item['label'])) {
                    $icon.= '<span class="legend">'.$item['label'].'</span>';
                }

                $class = isset($item['class']) ? $item['class'] : '';
                $classLink = '';

                if(!empty($item['requestedRoute']) && in_array(Yii::$app->requestedRoute, $item['requestedRoute'])){
                    $classLink = ' current ';
                }

                if(!empty($item['controller']) && Yii::$app->controller->id == $item['controller']){
                    $classLink = ' current ';
                }

                echo Html::tag('li', Html::a($icon, $item['url'], ['class' => $classLink]), ['title' => $item['label'], 'class' => 'carousel-cell' . ' ' . $class]);
            elseif(!isset($item['type'])) :
                echo Html::tag('li', $item['label'], ['class' => 'text-item carousel-cell']);
            endif;
        else :
            echo Html::tag('li', '', ['class' => 'separator']);
        endif;

        if(!empty($item['type']) && $item['type'] == 'submenu') {
            echo '
            <li class="header-submenu-content">
                <a id="' . $item['id'] . '" class="header-menu" data-toggle="' . $item['data-toggle'] . '" href="#">' . $item['label'] . ' <span class="fa fa-chevron-down"></span></a>
            </li>';

            printMenu($item['items']);

            echo '
                </ul>
            </li>';
        }
    endforeach;
}
?>

<div id="adminmenuback"></div>
<div class="menu-lateral">
    <ul class="carousel">
        <?php printMenu($sideMenu)?>
    </ul>
    <div class="clearfix"></div>
</div>
