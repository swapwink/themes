<?php
use common\models\Affiliate;
use common\models\Subsidiary;
use common\models\User;
use swapwink\common\ResourceImage;
use yii\helpers\Html;

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest) :
    if (in_array(Yii::$app->user->identity->user_type, [User::AFFILIATE, User::SUBSIDIARY])) :

        if (Yii::$app->user->identity->affiliate_id) :
            $affiliate = Affiliate::findOne(\Yii::$app->user->identity->affiliate_id);
        elseif (Yii::$app->user->identity->subsidiary_id) :
            $subsidiaryInfo = Subsidiary::findOne(Yii::$app->user->identity->subsidiary_id);
            $affiliate = Affiliate::findOne($subsidiaryInfo->affiliate_id);
        endif;

    elseif (Yii::$app->session->get('affiliate.id') != null) :
        $affiliate = Affiliate::findOne(\Yii::$app->session->get('affiliate.id'));
    endif;


    $credit = 0;

    if (Yii::$app->id != 'swapwink/mybusiness') {

        if (Yii::$app->user->identity->user_type == User::AFFILIATE) {

            $credit = Yii::$app->session['affiliate']->balance;

        } else {
            if(isset(Yii::$app->coreusers)){
                $credit = Yii::$app->coreusers->getCredit();
            }
        }
    } else {

        $classCredit = 'common\models\Credit';

        if (class_exists($classCredit)) {

            $creditModel = Yii::createObject($classCredit);

            if (Yii::$app->user->identity->user_type == User::AFFILIATE) {

                $credit = $creditModel->getCreditAvailableForAffiliate(Yii::$app->user->identity->affiliate_id);

            } else {

                $credit = $creditModel->getCreditAvailableForUser(Yii::$app->user->identity->id);
            }
        }

    }

    /**
     * TODO: Hacer el link dinamico, correspondiente
     *       a los instalados en el modulo moduleManager.
     */
    $optionsConfig = [
        'label' => Yii::t('commonTheme', 'Configuration'),
        'url' => '#',
        'icon' => 'fa-cog',
    ];

    $users = [
        'label' => Yii::t('commonTheme', 'Users'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
        'icon' => 'fa-user',
    ];

    $affiliates = [
        'label' => Yii::t('commonTheme', 'Affiliates'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
        'icon' => 'fa-building',
    ];

    $notifications = [
        'label' => Yii::t('commonTheme', 'Notifications'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('type-notification'),
        'icon' => 'fa-bell',
    ];

    $categories = [
        'label' => Yii::t('commonTheme', 'Categories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category'),
        'icon' => 'fa-list',
    ];

    $categoriesMarket = [
        'label' => Yii::t('commonTheme', 'Category'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category-market'),
        'icon' => 'fa-cubes',
    ];

    if(!Yii::$app->session->get('affiliate.isDevice')){
        $controlPanelMenu[] = [
            'label' => (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->user_type == User::SUBSIDIARY) ?  Yii::t('commonTheme', 'Settings') : Yii::t('commonTheme', 'General Profile'),
            'url' => Yii::$app->user->identity->user_type == User::AFFILIATE ? Yii::$app->urlManager->createUrl([
                'affiliate/view',
                'id' => Yii::$app->user->identity->affiliate_id
            ]) : (Yii::$app->user->identity->user_type == User::SUBSIDIARY ? Yii::$app->urlManager->createUrl([
                'subsidiary/view',
                'id' => Yii::$app->user->identity->subsidiary_id
            ]) : Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias)),
            'icon' => (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->user_type == User::SUBSIDIARY) ? 'fa-cog-swapwink' : 'fa-user-swapwink'
        ];
    }

    $moduleManager = [
        'label' => Yii::t('commonTheme', 'Module Manager'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('//moduleManager/default/index'),
        'icon' => 'fa-download',
    ];
    
    $currency = [
        'label' => Yii::t('commonTheme', 'Currency'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('currency'),
        'icon' => 'fa-money',
    ];
    
    $modules = [
        'label' => Yii::t('commonTheme', 'Module'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('module'),
        'icon' => 'fa-cubes',
    ];
    
    $recharge = [
        'label' => Yii::t('commonTheme', 'Credits'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge'),
        'icon' => 'fa-money'
    ];
    
    $couponcategory = [
        'label' => Yii::t('commonTheme', 'Coupon Categories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('couponcategory'),
        'icon' => 'fa-cubes',
    ];
    
    $promotionCode = [
        'label' => Yii::t('commonTheme', 'Promotion Code'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('recharge-promotion-code'),
        'icon' => 'fa-cubes',
    ];
    
    $promotionCodeWinks = [
        'label' => Yii::t('commonTheme', 'Promotion Code Winks'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('promotion-code-wink'),
        'icon' => 'fa-star',
    ];

    $winkstops = [
        'label' => Yii::t('commonTheme', 'Winkstops'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('winkstop'),
        'icon' => 'fa-flag',
    ];

    switch (Yii::$app->user->identity->user_type) :
        case User::ADMIN:
            $controlPanelMenu[] = $users;
            $controlPanelMenu[] = $affiliates;
            $controlPanelMenu[] = $currency;
            $controlPanelMenu[] = $modules;
            $controlPanelMenu[] = $notifications;
            $controlPanelMenu[] = $categories;
            $controlPanelMenu[] = $couponcategory;
            $controlPanelMenu[] = $categoriesMarket;
            $controlPanelMenu[] = $optionsConfig;
            $controlPanelMenu[] = $moduleManager;
            $controlPanelMenu[] = $promotionCode;
            $controlPanelMenu[] = $promotionCodeWinks;
            $controlPanelMenu[] = $winkstops;
            
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['backend/dashboard']);

        break;
        case User::PARTICIPANT:
            //TODO:menú de participante
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
        break;
        case User::AFFILIATE:
        case User::SUBSIDIARY:
        case User::DEVICE:
            //TODO:menú de afiliado
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['/']);
        break;
        case User::RECRUITER:
            //TODO:menú de reclutador
            $homeRedirect = Yii::$app->urlManager->createAbsoluteUrl(['recruiter/dashboard']);
        break;
    endswitch;

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => Yii::$app->user->identity->user_type == User::PARTICIPANT ? 'fa-sign-out-swapwink' : 'fa-sign-out-swapwink-green'
    ];
endif;
?>

<header class="main-header">
    <a href="<?= $homeRedirect ?>" class="logo-content">
        <?php
        $src = Yii::$app->params['cdnPathAdmin'] . "/img/logo-swap.png";
        if(!empty(Yii::$app->view->params['bodyClass']) && Yii::$app->view->params['bodyClass'] == 'view-affiliate-selector') {
            $src = Yii::$app->params['cdnPathAdmin'] . "/img/logo-swap-alternative.png";
        }
        ?>
        <?= Html::img($src) ?>
    </a>
    <div class="col-left hidden-xs hidden-sm">
        <h2><?= Yii::t('commonTheme', 'Administrative Panel') ?></h2>
        <p class="view-name"><?= Html::encode($titlePage) ?></p>
    </div>
    <div class="col-right">
        
        <?= $this->render('//layouts/menu-solutions', ['credit' => $credit]) ?>

        <div class="control-panel pull-right">
            <div class="navbar-right user-info">
                <?php
                if (\Yii::$app->user->identity->user_type == User::AFFILIATE) {
                    $name = $affiliate->affiliate_name;
                } else {
                    $name = !empty(\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name : \Yii::$app->user->identity->alias;
                    $name .= !empty(\Yii::$app->user->identity->last_name) ? ' ' . \Yii::$app->user->identity->last_name : '';
                } ?>

                <div class="user-avatar pull-left">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            <?php
                            if (in_array(Yii::$app->user->identity->user_type, [User::AFFILIATE, User::SUBSIDIARY]) || isset($affiliate)) :
                                echo Html::img($affiliate->urlLogo, ['id' => 'avatar', 'class' => 'user-avatar']);
                            else :
                                echo Html::img(Yii::$app->user->identity->urlAvatar, ['id' => 'avatar_profile', 'class' => 'user-avatar']);
                            endif; ?>
                            
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php if (!empty(Yii::$app->session->get('affiliate.id'))) : ?>
                                <li class="hidden-md hidden-lg">
                                    <div class="credit-amount">
                                        <h2><img src="<?= Yii::$app->params['cdn'] ?>/themes/default/img/credit.png"><?= number_format($credit) ?><span class="currency">swk</span></h2>
                                    </div>
                                </li>
                                <li class="hidden-md hidden-lg separator"></li>
                            <?php endif; ?>

                            <?php if (Yii::$app->user->identity->is_recruiter && (Yii::$app->user->identity->user_type != User::RECRUITER)): ?>
                                        
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?= Yii::$app->urlManager->createAbsoluteUrl([
                                            'recruiter/set'
                                        ]) ?>"><?= Yii::t('commonTheme', 'Recruiter') ?></a>
                                </li>

                                <li role="presentation" class="separator"></li>
                                
                            <?php endif; ?>

                            <?php
                            if (Yii::$app->session->get('affiliate.isDevice') == null && Yii::$app->user->identity->user_type != User::ADMIN) :
                                $modelUser = User::findIdentity(Yii::$app->user->identity->id);
                                if(empty(Yii::$app->view->params['bodyClass']) || Yii::$app->view->params['bodyClass'] != 'view-affiliate-selector') :
                                    if (count($modelUser->affiliates) != 0 || count($modelUser->activeSubsidiaries) != 0) :?>

                                        <li><h2><?= Yii::t('commonTheme', 'Use as:') ?></h2></li>
                                        <li class="separator"></li>
                                        <?php
                                        if (count($modelUser->affiliates) != 0) :?>
                                            <?php
                                            foreach ($modelUser->affiliates as $affiliate) :
                                                if (Yii::$app->user->identity->affiliate_id != $affiliate->id && $affiliate->status == Affiliate::ACTIVE) :?>
                                                    <li>
                                                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl([
                                                            'affiliate/set',
                                                            'id' => $affiliate->id
                                                        ]) ?>"><span class="logo-affiliate"><img src="<?=$affiliate->urlLogo?>"></span> <?= $affiliate->affiliate_name ?></a>
                                                    </li>
                                                    <?php
                                                endif;
                                            endforeach; ?>

                                            <?php
                                        endif; ?>

                                        <?php
                                        if (count($modelUser->activeSubsidiaries) != 0) :
                                            foreach ($modelUser->activeSubsidiaries as $subsidiary) :
                                                if (Yii::$app->user->identity->subsidiary_id != $subsidiary->id) :?>
                                                    <li role="presentation">
                                                        <a role="menuitem" tabindex="-1"
                                                           href="<?= Yii::$app->urlManager->createAbsoluteUrl([
                                                               'subsidiary/set',
                                                               'id' => $subsidiary->id
                                                           ]) ?>"><span class="logo-affiliate"><img src="<?= ResourceImage::getUrl($subsidiary->affiliate->logo, ResourceImage::LOGO) ?>"></span> Admin. <?= $subsidiary->subsidiary_name ?></a>
                                                    </li>
                                                    <?php
                                                endif;
                                            endforeach;
                                        endif; ?>
                                        <?php
                                    endif;?>

                                    <?php
                                    if (in_array(Yii::$app->user->identity->user_type, [User::AFFILIATE, User::RECRUITER]) || Yii::$app->user->identity->subsidiary_id != null) :
                                        
                                        if (Yii::$app->user->identity->user_type == User::AFFILIATE) :
                                            $controller = 'affiliate';
                                        elseif (Yii::$app->user->identity->user_type == User::RECRUITER) :
                                            $controller = 'recruiter';
                                        else :
                                            $controller = 'subsidiary';
                                        endif;
                                        
                                        $urlUnset = Yii::$app->urlManager->createAbsoluteUrl("{$controller}/unset");
                                        ?>
                                        <li class="separator"></li>
                                        <li>
                                            <a href="<?= $urlUnset ?>"><i class="fa fa-back-swapwink"></i><?=Yii::t('commonTheme', 'Back to Home');?></a>
                                        </li>
                                        <?php
                                    endif;
                                    
                                    echo Html::tag('li', '', ['class' => 'separator']);
                                endif;?>
                                <?php endif; ?>

                            <?php
                            foreach ($controlPanelMenu as $item) :
                                if (!isset($item['separator'])) :
                                    if (!empty($item['url'])) :
                                        $linkLabel = '<span class="fa ' . $item['icon'] . '"></span> ' . $item['label'];
                                        echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                                    else :
                                        echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                                    endif;
                                else :
                                    echo Html::tag('li', '', ['class' => 'separator']);
                                endif;
                            endforeach;
                            ?>
                        </ul><!--.dropdown-menu-->
                    </div><!--.dropdown-->
                </div><!--.user-avatar-->
            </div><!--.user-info-->
        </div><!--control-panel-->
    </div>
    <div class="clearfix"></div>
</header><!--.main-header-->
