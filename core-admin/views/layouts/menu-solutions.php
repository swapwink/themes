<?php
use common\models\User;

/* @var $this \yii\web\View */
/* @var $credit */

        if (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->user_type == User::SUBSIDIARY) : ?>
            <div class="dropdown hidden-md hidden-lg pull-left">
                <button id="btn-dropdown-menu-solutions" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= Yii::t('commonTheme', 'Solutions') ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-solutions" aria-labelledby="dLabel">
                    <li>
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//coupon/dashboard') ?>">
                            <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-coupon.png" ?>"> Swapwink
                        </a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//reward/dashboard') ?>">
                            <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-reward.png" ?>"> Rewards
                        </a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//ask/index') ?>">
                            <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-ask.png" ?>"> Ask
                        </a>
                    </li>
                    <?php
                    /* TODO: Habilitar cuando esté disponible la nueva versión de Contest
                    <li>
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl('//contest/index') ?>">
                            <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-contest.png"?>"> Contest
                        </a>
                    </li>
                    */
                    ?>
                </ul>
            </div><!--.dropdown-->
            <div class="pull-left solutions hidden-sm hidden-xs">
                <div class="solution-square">
                    <a class="link-solution <?= (Yii::$app->controller->id=="coupon") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//coupon/dashboard') ?>">
                        <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-coupon.png" ?>" class="solution"> <span class="solution-label">Swapwink</span>
                    </a>
                </div>
                <div class="solution-square">
                    <a class="link-solution <?= (Yii::$app->controller->id=="reward") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//reward/dashboard') ?>">
                        <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-reward.png" ?>" class="solution"> <span class="solution-label">Rewards</span>
                    </a>
                </div>
                <div class="solution-square">
                    <a class="link-solution <?= (Yii::$app->controller->id=="ask") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//ask/index') ?>" >
                        <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-ask.png" ?>" class="solution"> <span class="solution-label">Ask</span>
                    </a>
                </div>
                <?php 
                /* TODO: Habilitar cuando esté disponible la nueva versión de Contest
                <div class="solution-square">
                    <a class="link-solution <?= (Yii::$app->controller->id=="contest") ? "current" : "" ?>" href="<?= Yii::$app->urlManager->createAbsoluteUrl('//contest/index') ?>" >
                        <img src="<?= Yii::$app->params['cdnPathAdmin'] . "/img/ico-solution-contest.png"?>" class="solution"> <span class="solution-label">Contest</span>
                    </a>
                </div>
                */
                ?>     
            </div><!--.solutions-->
            <?php if (!empty(Yii::$app->session->get('affiliate.id'))) : ?>
                <div class="pull-left hidden-sm hidden-xs credit-amount">
                    <img src="<?= Yii::$app->params['cdn'] ?>/themes/default/img/credit.png"><?= $credit ?><span class="currency">swk</span>
                </div>
            <?php endif; ?>
        <?php endif; ?>