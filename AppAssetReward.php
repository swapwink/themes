<?php

namespace swapwink\themes;

use yii\web\AssetBundle;

class AppAssetReward extends AssetBundle
{
    public $sourcePath;
    public $basePath;
    public $baseUrl;
    public $css = [
        'css/main.less',
        '//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600',
        'https://d224v694zcq7e8.cloudfront.net/themes/default/css/main.css'
    ];
    public $js = [
        'js/functions.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

    public function init()
    {
        $this->sourcePath = !YII_DEBUG ? '@vendor/swapwink/themes/swapwink-bootstrap-reward' : null; //si esta definida realiza assetManager->publish()
        $this->basePath = !YII_DEBUG ? null : '@vendor/swapwink/themes/swapwink-bootstrap-reward';
        $this->baseUrl = !YII_DEBUG ? null : '/vendor/swapwink/themes/swapwink-bootstrap-reward/';

        parent::init();

        // restablecer BootstrapAsset para que no cargue sus archivos js que causan conflicto
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'js' => []
        ];
    }
}
