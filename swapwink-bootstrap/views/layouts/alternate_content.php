<?php
use swapwink\themes\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage ?>" lang="<?= Yii::$app->language ?>">
<?= $this->render('//layouts/header') ?>
<body class="body-login">
    <?php $this->beginBody() ?>
	<?= $content ?>
    <div class="footer-login">
    	&copy; <?= date('Y') ?> SwapWink&reg;. <?= Yii::t('commonTheme', 'All rights reserved') ?>.
	</div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
