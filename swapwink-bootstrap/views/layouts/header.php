<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->title = Yii::$app->name . ' - ' .  ucfirst(Yii::$app->controller->id) . " " . ucfirst(Yii::$app->controller->action->id);
if(isset($this->params['pageTitle'])){
    $this->title = $this->params['pageTitle'];
}
/* Modifica el color del tema del navegador chrome */
$this->registerMetaTag(['name' => 'theme-color', 'content' => '#00dca2']);
?>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<?php
		$this->head();
		$this->registerLinkTag(['rel' => 'icon', 'href' => Yii::$app->params['cdnPath'] . '/img/favicon.ico']);
	?>

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>