<?php

use common\models\Affiliate;
use common\models\User;
use yii\helpers\Html;

$controlPanelMenu = [];

if (!\Yii::$app->user->isGuest):

    $optionsConfig = [
        'label' => Yii::t('commonTheme', 'Configuration'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('optionsConfig'),
        'icon' => 'fa-cog',
    ];

    $users = [
        'label' => Yii::t('commonTheme', 'Users'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('user'),
        'icon' => 'fa-user',
    ];

    $affiliates = [
        'label' => Yii::t('commonTheme', 'Affiliates'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('affiliate'),
        'icon' => 'fa-building',
    ];

    $notifications = [
        'label' => Yii::t('commonTheme', 'Notifications'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('typeNotification'),
        'icon' => 'fa-bell',
    ];
    
    /*
     * Opción solo de social
     */
    $categories = [
        'label' => Yii::t('commonTheme', 'Categories'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category'),
        'icon' => 'fa-list',
    ];

    $categoriesMarket = [
        'label' => Yii::t('commonTheme', 'Category'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('category-market'),
        'icon' => 'fa-cubes',
    ];
    
    /**
     * Opción de social y cupon
     */
    $subsidiary = [
        'label' => Yii::t('commonTheme', 'Locations'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('subsidiary'),
        'icon' => 'fa-simplybuilt',
    ];
	
    $myCoupons = [
        'label' => Yii::t('commonTheme','My Coupons'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('coupon/myCoupons'),
        'icon' => 'my-coupons',
    ];

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Profile'),
        'url' => Yii::$app->user->identity->user_type == User::AFFILIATE ? Yii::$app->urlManager->createUrl(['affiliate/view', 'id' => Yii::$app->user->identity->affiliate_id]) : Yii::$app->urlManager->createAbsoluteUrl("@" . Yii::$app->user->identity->alias),
        'icon' => 'fa-user'
    ];

    switch (Yii::$app->user->identity->user_type):
        case User::ADMIN:
            $controlPanelMenu[] = $users;
            $controlPanelMenu[] = $affiliates;
            $controlPanelMenu[] = $notifications;
            //$controlPanelMenu[] = $categories; //Solo sirve para social
            $controlPanelMenu[] = $categoriesMarket;
            $controlPanelMenu[] = $optionsConfig;

            break;
        case User::PARTICIPANT:
            if(Yii::$app->id == 'swapwink/coupon'){
                    $controlPanelMenu[] = $myCoupons;
            }
            //TODO:menú de participante
            break;
        case User::AFFILIATE:
            $affiliate = Affiliate::findOne(\Yii::$app->user->identity->affiliate_id);
            
            //TODO:menú de afiliado
            $controlPanelMenu[] = $subsidiary;
            $controlPanelMenu[] = [
                'label' => Yii::t('commonTheme', 'Credits'),
                'url' => Yii::$app->urlManager->createAbsoluteUrl('credit'),
                'icon' => 'fa-credit-card'
            ];
        
            break;
    endswitch;

    $controlPanelMenu[] = [
        'label' => Yii::t('commonTheme', 'Log out'),
        'url' => Yii::$app->urlManager->createAbsoluteUrl('site/logout'),
        'icon' => 'fa-sign-out'
    ];

$numNotifications = Yii::$app->notifier->count();

endif;
?>

<div class="navbar-wrapper navbar">
    <div class="navbar-head">
        <a id="home_url" href="<?= Yii::$app->homeUrl ?>">
            <?= Html::img(Yii::$app->params['cdnPath'] . "/img/logo-header.png", ['class' => 'navbar-brand', 'alt' => Yii::$app->name]); ?>
        </a>

<?php if (!\Yii::$app->user->isGuest): ?>

            <div class="control-panel pull-right">
                <div class="user-info button_switch">
                    <?php
                    if (\Yii::$app->user->identity->user_type == User::AFFILIATE) {
                        $name = $affiliate->affiliate_name;
                    } else {
                        $name = !empty(\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name : \Yii::$app->user->identity->alias;
                        $name .=!empty(\Yii::$app->user->identity->last_name) ? ' ' . \Yii::$app->user->identity->last_name : '';
                    }
                    ?>
                    <p class="pull-left user-name"><span class="name"><?= ucwords($name) ?></span><i class="fa fa-caret-down"></i></p>

                    <div class="user-avatar pull-left">
                        <?= (\Yii::$app->user->identity->user_type == User::AFFILIATE) ? Html::img($affiliate->urlLogo, ['id' => 'avatar', 'class' => 'user-avatar']) : Html::img(Yii::$app->user->identity->urlAvatar, ['id' => 'avatar_profile', 'class' => 'user-avatar'])?>
                    </div>
                </div><!--.user-info-->
                <?php $modelUser = User::findIdentity(Yii::$app->user->identity->id); ?>
                <?= $this->$this->render('//layouts/menu', ['modelUser' => $modelUser, 'controlPanelMenu' => $controlPanelMenu]); ?>
            </div><!--.control-panel-->

            <div class="container-messages-notifications">

                <div class="notification-header container-messages">
                    <a class="notification-head navbar-buttons" data-actions='{"load":"<?= Yii::$app->notifier->generateUrl('preview') ?>", "ajaxURL":"<?= Yii::$app->notifier->generateUrl('check-readed') ?>", "crossDomain":"<?= Yii::$app->notifier->acceptCrossDomain() ?>"}'>
                        <span class="fa fa-bell"></span>
                        <span id="numNotification" class="label"><?= ($numNotifications > 0) ? $numNotifications : '' ?></span>
                    </a>
                    <div class="box-messages notification-requests">
                        <span class="arrow"></span>
                        <h3><?= Yii::t('commonTheme', 'Notifications') ?></h3>
                        <div class="request-list"></div>
                        <div class='view-all'><?= Html::a(Yii::t('commonTheme', 'view all'), Yii::$app->urlManager->createUrl('notifier/index')) ?></div>
                    </div><!--.notification-requests-->
                </div><!--.notification-header-->

            </div><!--.container-messages-notifications-->
			
            <div class="container-messages-notifications">
                    <a class="navbar-buttons" href="<?= Yii::$app->homeUrl ?>">
                            <span class="fa fa-home"></span>
                    </a>
            </div>
    <?php else: ?>
        <?php
        if (!empty(Yii::$app->session->get('preregister'))) :
            ?>
            <div class="control-guest pull-right">
                <a href="#" class="a-guest" style="text-align: right">
                    <?= Yii::$app->session->get('preregister')->first_name ?>
                    <?= Yii::$app->session->get('preregister')->last_name ?>
                    <br>
                    <?= Yii::$app->session->get('preregister')->email ?>
                </a>

            </div>
            <?php
        else :
            ?>
            <div class="control-guest pull-right">
                <a href="<?= Yii::$app->homeUrl ?>" class="a-guest">
                    <?= Yii::t('commonTheme', 'Login / Register') ?>
                </a>
            </div><!--.control-panel-->
            <?php
        endif;
        ?>
    <?php endif; ?>
    </div><!--.navbar-head-->

</div><!--.navbar-wrapper-->
