<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div class="content container">
        <?= $content ?>
    </div>

<?php $this->endContent(); ?>

