<?php

use common\models\User;
use common\widgets\Alert;
use swapwink\themes\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);

$location = (isset($this->params['location']))? $this->params['location'] : null;
$languageInfo = User::getInfoToggleLanguage();
$urlChangeLang = Url::toRoute(['/site/changeLanguage', 'lang' => $languageInfo['language']]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::$app->sourceLanguage; ?>" lang="<?= Yii::$app->language ?>">
<?= $this->render('//layouts/header') ?>
<?php if (in_array($this->context->action->id, ["login"])):?>
<body class="body-login">
    <?php $this->beginBody() ?>
        <?= $content ?>
            <div class="footer-login">
                
		&copy; <?= date('Y') ?> SwapWink&reg;. <?= Yii::t('commonTheme', 'All rights reserved') ?>.
                <?= Html::a(Yii::t('commonTheme', 'Privacy'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/privacy')) :  Yii::$app->homeUrl,['style'=>'color:#fff'])?>.
                <?= Html::a(Yii::t('commonTheme', 'Terms'), !empty(Yii::$app->getModule('moduleManager')->getModule('siteModule')) ? Url::to(Yii::$app->urlManager->createUrl('site/terms')) :  Yii::$app->homeUrl,['style'=>'color:#fff'])?>
                <!--// TODO: Quitar style poner en css -->
                <div class="language-menu">
                    <div class="dropdown">
                        <a id="dLabel" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="<?= User::getInfoLanguage()['icon'] ?>"></div>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-top" aria-labelledby="dLabel">
                            <li>
                                <a href="<?= $urlChangeLang ?>" class="<?= $languageInfo['icon'] ?>">
                                    <div class="<?= $languageInfo['icon'] ?>"> </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    <?php $this->endBody() ?>
</body>
<?php else: ?>
<body>
    <?php $this->beginBody() ?>
        <?= $this->render('//layouts/navhead') ?>
        <?= Alert::widget() ?>
	<?= $content ?>
    <?= $this->render('//layouts/footer') ?>
    <?php $this->endBody() ?>
</body>
<?php endif; ?>
</html>
<?php $this->endPage() ?>
