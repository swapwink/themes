<div class="drop-list">
    <ul>
        <?php
        
        if (count($modelUser->affiliates) > 0 || count($modelUser->subsidiaries) > 0):
            ?>

            <li><h2><?= Yii::t('commonTheme', 'Use SwapWink as:') ?></h2></li>

            <?php if (count($modelUser->affiliates) > 0): ?>
                <?php
                foreach ($modelUser->affiliates as $affiliate):
                    if (Yii::$app->user->identity->affiliate_id != $affiliate->id):
                        if ((isset(Yii::$app->params['show-menu-core']) && $affiliate->status == Affiliate::ACTIVE) || (isset($affiliate->status_module) && $affiliate->status_module == Affiliate::STATUS_ACTIVE)):
                            ?>
                            <li>
                                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['affiliate/set', 'id' => $affiliate->id]) ?>" ><?= $affiliate->affiliate_name ?></a>
                            </li>
                            <?php
                        endif;
                    endif;
                endforeach;
                ?>

            <?php endif; ?>

            <?php
            if (count($modelUser->subsidiaries) != 0):
                foreach ($modelUser->subsidiaries as $subsidiary):
                    if (Yii::$app->user->identity->subsidiary_id != $subsidiary->id):
                        ?>
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['subsidiary/set', 'id' => $subsidiary->id]) ?>" >Admin. <?= $subsidiary->subsidiary_name ?></a>
                        </li>
                        <?php
                    endif;
                endforeach;
            endif;
            ?>

            <?php if (Yii::$app->user->identity->user_type == User::AFFILIATE || Yii::$app->user->identity->subsidiary_id != null): ?>
                <li>
                    <a href="<?= (Yii::$app->user->identity->subsidiary_id == null) ? Yii::$app->urlManager->createAbsoluteUrl('affiliate/unset') : Yii::$app->urlManager->createAbsoluteUrl('subsidiary/unset'); ?>"><?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name ?></a>
                </li>
            <?php endif; ?>
        <?php endif; ?>

        <?= Html::tag('li', '', ['class' => 'separator']) ?>

        <?php
        foreach ($controlPanelMenu as $item):
            if (!isset($item['separator'])):
                if (!empty($item['url'])):
                    $linkLabel = '<span class="fa ' . $item['icon'] . '"></span>' . $item['label'];
                    echo Html::tag('li', Html::a($linkLabel, $item['url']), []);
                else:
                    echo Html::tag('li', $item['label'], ['class' => 'text-item']);
                endif;
            else:
                echo Html::tag('li', '', ['class' => 'separator']);
            endif;
        endforeach;
        ?>
    </ul>
    <span class="arrow"></span>
</div><!--.drop-list-->