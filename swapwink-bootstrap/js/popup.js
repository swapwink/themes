/* funcion para actualizar valores de una url*/

function UpdateQueryString(key, value, url) {
    if (!url)
        url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
            hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    } else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        } else
            return url;
    }
}

$(document).ready(function () {
    document.onkeydown = key;
});

$(document).on("click", "#background_popup, #close_popup, .close_popup, .cancel_popup", function () {
    OnOffBox("hide_popup", false);
});


var is_register = false;

function assign_events(id) {

    var selector = id;
    var expressionClass = new RegExp('^[.]');
    var expressionId = new RegExp('/^\#/');

    if (!expressionClass.test(id) && !expressionId.test(id)) {
        selector = '#' + id;
    }

    $(selector).on("click", function (e) {
        e.preventDefault();

        if ($(this).hasClass("iframe")) {

            var url = UpdateQueryString("m", "iframe", $(this).attr('href'));

            $('#content_popup').html("<iframe src='" + url + "' width='100%' scrolling='no' frameBorder='0' name='" + id + "'>");
            OnOffBox("show_popup", true);

            iFrameResize({
                log: false, // Enable console logging
                checkOrigin: false,
                inPageLinks: true,
                messageCallback: function (messageData) { // Callback fn when message is received
                    messageData = JSON.parse(messageData.message);

                    if (messageData.action == "closeFrame") {
                        OnOffBox("hide_popup", true);
                    }

                    if (messageData.action == "refreshParent") {
                        location.href = UpdateQueryString("a", messageData.nextUrl, location.href);
                    }
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: $(this).attr('href'),
                success: function (returnedData) {
                    $('#content_popup').html(returnedData);

                    OnOffBox("show_popup", true);
                }
            });
        }

        return false;
    });

}

var key = function (e) {
    e = e || window.event;
    var k = e.keyCode || e.which;
    if (k == 27) {
        OnOffBox("hide_popup", false);
    }
};

function submit_form(Form, data, hasError) {
    if (!hasError) {
        $.ajax({
            url: Form.attr("action"),
            type: Form.attr("method"),
            data: Form.serialize(),
            success: function (returnedData) {
                $('#content_popup').html(returnedData);
            },
            beforeSend: function (returnedData) {
                if (document.getElementById("reload")) {
                    $('#reload').html('<i class="fa fa-spinner fa-spin fa-pulse fa-3x fa-fw margin-bottom"></i>');
                    $("input[type=submit]", Form).attr("disabled", "disabled");
                }
            },
            error: function (returnedData) {
                console.log(returnedData);
                return false;
            }
        });
    }
    return false;
}

function OnOffBox(op, is_register_op)
{
    document.getElementById("background_popup").className = op;
    document.getElementById("container_popup").className = op;
    is_register = is_register_op;
}